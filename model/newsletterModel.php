<?php

class newsletterModel extends Model {

	public $options = [ 'Table' => 'newsletter', 'Redirect' => 'newsletter', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		if( $_POST['newsletter'] ) {
			$email = trim( strip_tags( $_POST['newsletter'] ) );
			if( !$email ) {
				$_SESSION[I_ERROR] = $this->data['dictionary'][112][LANG];
				header("Location: " . $_SERVER['HTTP_REFERER'] );
				exit();
			}

			$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE email = :email LIMIT 1" );
			$sth->execute( [':email' => $email] );
			if( $sth->rowCount() > 0 ) {
				$_SESSION[I_ERROR] = $this->data['dictionary'][113][LANG];
				header("Location: " . $_SERVER['HTTP_REFERER'] );
				exit();
			}
			$token = hash( 'sha256' , $email . time() );
			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} (email, data_zapisu, data_potwierdzenia, token, stat) VALUES(:email, NOW(), NULL, '{$token}', '0')" );
			if( !$sth->execute([':email' => $email]) ) {
				$_SESSION[I_ERROR] = $this->data['dictionary'][114][LANG];
				header("Location: " . $_SERVER['HTTP_REFERER'] );
				exit();
			}


			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->SMTPAuth = true;
			$mail->addReplyTo( MAIL_FROM, MAIL_FROM_NAME );
			$mail->addAddress( $email, '');
			$mail->Subject = $this->data['dictionary'][58][LANG];

			$body = @file_get_contents( 'public/mail/mail.html' );
			$body = preg_replace( "/{{BASE}}/" , BASE, $body );
			$body = preg_replace( "/{{CONTENT}}/" , $this->data['dictionary'][59][LANG], $body );
			$cta = "<table width='600' align='center' border='0' cellspacing='0' cellpadding='0' style='margin: 0 auto;'>
						<tr>
							<td height='30' width='200'></td>
							<td height='30' width='200' bgcolor='#e3010f' style='font-family: Verdana; font-size: 13px; vertical-align: middle; text-align: center; color: #f1f1f1; text-decoration: none;'>
								{{LINK}}
							</td>
							<td height='30' width='200'></td>
						</tr>
					</table>";
			if( $cta )
				$body = preg_replace( "/{{CTA}}/" , $cta, $body );
			else
				$body = preg_replace( "/{{CTA}}/" , "", $body );
			
			$body = preg_replace( "/{{FB}}/" , $this->data['dictionary'][22][LANG], $body );
			$link = "<a href='" . BASE . "newsletter/potwierdz/?token={$token}' style='color:#f1f1f1;'><strong>POTWIERDŹ</strong></a>";
			$body = preg_replace( "/{{LINK}}/" , $link, $body );
			$mail->msgHTML( $body );
			if (!$mail->send())
			    throw new modelException( $this->data['dictionary'][115][LANG], 4016 );


			$_SESSION[I_SUCCESS] = $this->data['dictionary'][116][LANG];
			if( $_SERVER['HTTP_REFERER'] )
				header("Location: " . $_SERVER['HTTP_REFERER'] );
			else
				header("Location: " . BASE . $this->options['Redirect'] );
			exit();
		} else {
			header( 'Location: ' . BASE );
			exit();
		}
	}

	function potwierdz() {
		if( !$_GET['token'] ) {
			$_SESSION[I_ERROR] = $this->data['dictionary'][117][LANG];
			header("Location: " . BASE );
			exit();
		}

		$token = strip_tags( trim( $_GET['token'] ) );
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE token = '{$token}' LIMIT 1" );
		$sth->execute();
		$this->data['newsletter'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( !$this->data['newsletter'] ) {
			$_SESSION[I_ERROR] = $this->data['dictionary'][118][LANG];
			header("Location: " . BASE );
			exit();
		}

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1', data_potwierdzenia = NOW() WHERE id = {$this->data['newsletter']['id']}" );
		$sth->execute();

		$_SESSION[I_SUCCESS] = $this->data['dictionary'][119][LANG];
		header("Location: " . BASE );
		exit();

	}

}
