<?php

use FreshMail\RestApi;
class indexModel extends Model {
	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$options['Table'] = 'artykuly';
		$options['SliderCount'] = 3;
		$options['SubSliderCount'] = 2;
		// slider
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$options['Table']} WHERE stat = '1' AND slider = '1' ORDER BY slider_pozycja LIMIT {$options['SliderCount']}" );
		$sth->execute();
		$this->data['slider'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// podslider
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$options['Table']} WHERE stat = '1' AND sub_slider = '1' ORDER BY pozycja LIMIT {$options['SubSliderCount']}" );
		$sth->execute();
		$this->data['sub_slider'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// artykuły
		$sth = $this->pdo->prepare( "SELECT * FROM ( SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$options['Table']} WHERE stat = '1' UNION SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM newsroom WHERE strona_glowna = '1' AND stat = '1' ) as subquery ORDER BY data_dodania DESC, pozycja LIMIT 6" );
		$sth->execute();
		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// kalendarium
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_wydarzenia, '%d.%m.%Y' ) as data FROM wydarzenia WHERE stat = '1' ORDER BY pozycja LIMIT 3" );
		$sth->execute();
		$this->data['wydarzenia'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// rok start 
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_wydarzenia, '%Y' ) as rok FROM wydarzenia WHERE stat = '1' ORDER BY DATE_FORMAT( data_wydarzenia, '%Y' ) LIMIT 1" );
		$sth->execute();
		$this->data['rok'] = $sth->fetch( PDO::FETCH_ASSOC );

	}

	function admin() {
		$this->data['admin'] = true;
		if( Auth::ifAuth() ) {
			header( "Location: " . BASE . "admin" );
			exit();
		}

		if( Routing::$routing['param'] == 'pl' || Routing::$routing['param'] == 'en' ) {
			setcookie( COOKIE_LANG_NAME, Routing::$routing['param'], time()+157680000, '/' );
			header( "Location: " . BASE );
			exit();
		}

		if( $_POST['remind'] == 1 ) {
			$email = $_POST['email'];

			$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE email = :email LIMIT 1" );
			$sth->execute( array( ':email' => $email ) );
			if( $sth->rowCount() > 0 ) {
				$this->data['u'] = $sth->fetch( PDO::FETCH_ASSOC );
				$token = $this->data['u']['token'];
				// $hashpass = substr( hash('sha512', rand()),0, 10 );
				// $newpass = sha1( $hashpass );
				// $token = sha1( $newpass . time() . $this->data['u']['id'] );
				// $sth = $this->pdo->prepare( "UPDATE uzytkownicy SET haslo = '{$newpass}', token = '{$token}' WHERE id = {$this->data['u']['id']}" );
				// $sth->execute();
				$l = $this->data['u']['lang'];
				$_SESSION[I_SUCCESS] = $this->data['dictionary'][136][$l];
				$mail = new PHPMailer();
				$body = file_get_contents( 'public/mailing/mailing.html' );
				$cta =  BASE . "profil/haslo/" . $this->data['u']['id'] . "/" . $token;
				$body = preg_replace( "/{{CONTENT}}/" , $this->data['dictionary'][134][$l], $body );
				$body = preg_replace( "/{{LANG}}/" , $l, $body );
				$body = preg_replace( "/{{BASE}}/" , BASE, $body );
				$body = preg_replace( "/{{CTA}}/" , $cta, $body );
				$body = preg_replace( "/{{PREFIX}}/" , 'remind', $body );
				$body = preg_replace( "/{{ALT}}/" , $this->data['dictionary'][57][LANG], $body );
				$mail->IsSMTP();
				$mail->SMTPDebug  = 0;
				$mail->SMTPAuth   = true;
				$mail->Subject    = $this->data['dictionary'][135][$l];
				$mail->MsgHTML($body);
				$mail->AddAddress( $this->data['u']['email'] );

				if( !$mail->Send() ) {
					echo "Mailer Error: " . $mail->ErrorInfo;
				}
					
			} else {
				$_SESSION[I_ERROR] = $this->data['dictionary'][6][LANG];
			}

			header("Location: " . BASE . Routing::$routing['controller']);
			exit();
		}

		if( $_POST['login-send'] == 1 ) {

			$login = ( $_POST['login'] ) ? trim( strip_tags( $_POST['login'] ) ) : null;
			$haslo = ( $_POST['haslo'] ) ? sha1( trim( strip_tags( $_POST['haslo'] ) ) ) : null;

			if( !$login )
				throw new modelException( $this->data['dictionary'][7][LANG], 4007 );

			if( !$haslo )
				throw new modelException( $this->data['dictionary'][8][LANG], 4008 );

			// $sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( ostatnie_logowanie, '%d-%m-%Y %H:%i') as ostatnie_logowanie_format FROM users WHERE email = :login AND haslo='{$haslo}' AND stat = '1' LIMIT 1" );
			$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( ostatnie_logowanie, '%d-%m-%Y %H:%i') as ostatnie_logowanie_format FROM " . ADMIN_TABLE . " WHERE email = :login AND haslo='{$haslo}' AND stat = '1' LIMIT 1" );
			$sth->bindParam( ':login', $login, PDO::PARAM_STR );
			$sth->execute();
			$row = $sth->fetch();
			
			if( !$row )
				throw new modelException( $this->data['dictionary'][9][LANG], 4009 );

			if( $row['newsroom_klient_id'] != NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM firmy WHERE stat = '1' AND id = {$row['newsroom_klient_id']} LIMIT 1" );
				$sth->execute();
				if( $sth->rowCount() < 1 )
					throw new modelException( $this->data['dictionary'][45][LANG], 4009 );
			}

			Auth::register( $row );
			$_SESSION[I_SUCCESS] = "Poprawnie zalogowano do panelu";
			// $sth = $this->pdo->prepare( "UPDATE " . ADMIN_TABLE . " SET ostatnie_logowanie = NOW(), pierwsze_logowanie = '1' WHERE id = {$row['id']}" );
			$sth = $this->pdo->prepare( "UPDATE " . ADMIN_TABLE . " SET ostatnie_logowanie = NOW() WHERE id = {$row['id']}" );
			$sth->execute();

			if( $row['pierwsze_logowanie'] == '0' ) {
				header( "Location: " . BASE . "profil/haslo/" . $row['id'] );
				exit();
			}

			if( $_SESSION['redirect'] && $_SESSION['redirect'] != BASE . 'konto/login' ) {
				header( "Location: " . $_SESSION['redirect'] );
			} else {
				header( "Location: " . BASE . "admin" );
			}
			$_SESSION['redirect'] = null;
			exit();
		}
	}

	function video() {
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "SELECT * FROM video WHERE id = {$id} LIMIT 1" );
		$sth->execute();
		$this->data['film'] = $sth->fetch( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT * FROM video WHERE stat = '1' ORDER BY id DESC LIMIT 45" );
		$sth->execute();
		$this->data['last'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data, '%d/%m/%Y %H:%i' ) as data FROM komentarze WHERE id_filmu = {$id} AND stat = '1' ORDER BY id DESC" );
		$sth->execute();
		$this->data['comments'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	function szukaj() {
		if( !$_GET['szukaj'] ) {
			$this->data['results'] = [];
			return;
		}

		$this->data['search'] = $search = strip_tags( trim( $_GET['szukaj'] ) );

		// szukam artykułów
		$sth = $this->pdo->prepare( "SELECT *, 'artykul' as _TYPE_LINK_ FROM artykuly WHERE stat = '1' AND (tytul_" . LANG . " REGEXP '{$search}' OR tresc_" . LANG . " REGEXP '{$search}' OR zajawka_" . LANG . " REGEXP '{$search}')" );
		$sth->execute();
		$this->data['aktualnosci'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// szukam newsroom
		$sth = $this->pdo->prepare( "SELECT *, 'newsroom' as _TYPE_LINK_ FROM newsroom WHERE stat = '1' AND (tytul_" . LANG . " REGEXP '{$search}' OR tresc_" . LANG . " REGEXP '{$search}' OR zajawka_" . LANG . " REGEXP '{$search}')" );
		$sth->execute();
		$this->data['newsroom'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// szukam wydarzeń
		$sth = $this->pdo->prepare( "SELECT *, 'wydarzenia' as _TYPE_LINK_ FROM wydarzenia WHERE stat = '1' AND (tytul_" . LANG . " REGEXP '{$search}' OR tresc_" . LANG . " REGEXP '{$search}')" );
		$sth->execute();
		$this->data['wydarzenia'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// szukam stron
		$sth = $this->pdo->prepare( "SELECT *, 'strona' as _TYPE_LINK_ FROM strony WHERE stat = '1' AND (tytul_" . LANG . " REGEXP '{$search}' OR tresc_" . LANG . " REGEXP '{$search}')" );
		$sth->execute();
		$this->data['strony'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$this->data['results'] = array_merge( $this->data['aktualnosci'], $this->data['newsroom'], $this->data['wydarzenia'], $this->data['strony'] );
	}
}
