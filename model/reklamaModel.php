<?php

class reklamaModel extends Model {


	public $options = [ 'Table' => 'reklama', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE);
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT * FROM reklama WHERE id = {$id} LIMIT 1" );
		$sth->execute();
		$this->data['reklama'] = $sth->fetch( PDO::FETCH_ASSOC );
		if( !$this->data['reklama'] ) {
			header("Location: " . BASE);
			exit();
		}

		// dodaję liczbę wyswietleń artykułu i ustawiam ciasteczko
		$cookie = ( $_COOKIE[COOKIES_ADV] ) ? unserialize( $_COOKIE[COOKIES_ADV] ) : [];
		if( $cookie ) {
			$show = $this->data['reklama']['klikniecia'] + 1;
			if( !in_array( $id , $cookie ) ) {
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET klikniecia = {$show} WHERE id = {$id}" );
				$sth->execute();
				array_push( $cookie , $id );
				$new_cookie = serialize( $cookie );
				setcookie( COOKIES_ADV, $new_cookie, time() + (60 * COOKIES_ADV_TIMEOUT), "/");
			}
		} else {
			$show = (int)$this->data['reklama']['klikniecia'] + 1;
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET klikniecia = {$show} WHERE id = {$id}" );
			$sth->execute();
			array_push( $cookie , $id );
			$new_cookie = serialize( $cookie );
			setcookie( COOKIES_ADV, $new_cookie, time() + (60 * COOKIES_ADV_TIMEOUT), "/");
		}


		header( "Location: " . $this->data['reklama']['link'] );
		exit();

	}

}
