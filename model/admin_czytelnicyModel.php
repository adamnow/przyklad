<?php

class admin_czytelnicyModel extends Model {

	public $options = [ 'Table' => 'czytelnicy', 'Redirect' => 'admin_czytelnicy', 'SearchCol' => 'tytul_pl',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		// $this->options['SearchCol'] = 'imie' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " WHERE imie REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "' OR nazwisko REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "' OR email REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} {$search} ORDER BY email LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

	function wlacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie włączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z włączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function wylacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '0' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie wyłączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function usun() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$c = Routing::$routing['title'];

		if( $c != 'confirm' ) {
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT image, pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['pozycja'];

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE pozycja > $next" );
		$sth->execute();

		$sth = $this->pdo->prepare( "DELETE FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 ) {
			$_SESSION[I_SUCCESS] = "Poprawnie usunięto wpis";
			// @unlink( "userfiles/images/artykuly/" . $this->data['pos']['image'] );
		} else {
			$_SESSION[I_ERROR] = "Wystąpił problem z usunięciem wpisu";
		}

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function edytuj() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );
		
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);
			
		$this->data['czytelnik'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( $_POST['edit'] == 1 ) {

			$this->data['imie'] = strip_tags( trim( $_POST['imie'] ) );
			$this->data['nazwisko'] = trim( $_POST['nazwisko'] );
			$this->data['firma'] = trim( $_POST['firma'] );
			$this->data['stanowisko'] = trim( $_POST['stanowisko'] );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET imie = :imie, nazwisko = :nazwisko, firma = :firma, stanowisko = :stanowisko, stat = '{$this->data[stat]}' WHERE id = {$id}" );
			
			if( !$sth->execute( 
				[
					':imie' => $this->data['imie'], 
					':nazwisko' => $this->data['nazwisko'], 
					':firma' => $this->data['firma'],
					':stanowisko' => $this->data['stanowisko'],
				] ) ) {
					throw new modelException( "Wystąpił problem z edycją wpisu", 1 );
				}

				$_SESSION[I_SUCCESS] = "Poprawnie edytowano wpis";
				header( "Location: " . BASE . $this->options['Redirect'] );
				exit();
		}
	}

	function dodaj() {
		$this->getUser();

		if( $_POST['add'] == 1 ) {

			$this->data['email'] = strip_tags( trim( $_POST['email'] ) );
			$this->data['imie'] = strip_tags( trim( $_POST['imie'] ) );
			$this->data['nazwisko'] = trim( $_POST['nazwisko'] );
			$this->data['firma'] = trim( $_POST['firma'] );
			$this->data['stanowisko'] = trim( $_POST['stanowisko'] );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE email = :email LIMIT 1" );
			$sth->execute( [':email' => $this->data['email'] ] );
			if( $sth->rowCount() > 0 )
				throw new modelException( "Czytelnik o tym adresie email istnieje już w bazie. Nie możesz dodać go ponownie", 1 );
				

			// dodaję artykuł do bazy
			$token = hash( 'sha256' , $this->data['email'] . $this->data['imie'] . $this->data['nazwisko'] . time() );
			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} ( imie, nazwisko, email, firma, stanowisko, data_dodania, token, stat ) VALUES( :imie, :nazwisko, :email, :firma, :stanowisko, NOW(), '{$token}', '{$this->data[stat]}' )" );
			
			if( !$sth->execute( 
				[
					':imie' => $this->data['imie'], 
					':nazwisko' => $this->data['nazwisko'], 
					':email' => $this->data['email'], 
					':firma' => $this->data['firma'],
					':stanowisko' => $this->data['stanowisko'],
				] ) ) {
					throw new modelException( "Wystąpił problem z edycją wpisu", 1 );
				}

				$_SESSION[I_SUCCESS] = "Poprawnie dodano wpis";
				header( "Location: " . BASE . $this->options['Redirect'] );
				exit();
		}
	}

	function odwiedziny() {
		
		$this->getUser();
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);
			
		$this->data['czytelnik'] = $sth->fetch( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT * FROM czytelnicy_odwiedziny WHERE id_czytelnika = {$this->data['czytelnik']['id']} ORDER BY data_wizyty DESC" );
		$sth->execute();
		$this->data['odwiedziny'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	function xls() {
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} ORDER BY email" );
		$sth->execute();
		$this->data['u'] = $sth->fetchAll( PDO::FETCH_ASSOC );


		$objPHPExcel = new PHPExcel();
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'imie')
		            ->setCellValue('B1', 'nazwisko')
		            ->setCellValue('C1', 'email')
		            ->setCellValue('D1', 'firma')
		            ->setCellValue('E1', 'stanowisko')
		            ->setCellValue('F1', 'data_dodania')
		            ->setCellValue('G1', 'status');

		$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$row = 1;
		foreach( $this->data['u'] as $item ) {
			$row++;
			$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValueExplicit('A' . $row , $item['imie'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('B' . $row , $item['nazwisko'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('C' . $row , $item['email'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('D' . $row , $item['firma'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('E' . $row , $item['stanowisko'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('F' . $row , $item['data_dodania'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('G' . $row, $s = ( $item['stat'] == 1 ) ? 'aktywny' : 'nieaktywny', PHPExcel_Cell_DataType::TYPE_STRING );
		}

		$objPHPExcel->getActiveSheet()->setTitle( 'czytelnicy-' . date('Ymd') );
		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="czytelnicy-' . date( 'Ymd' ) . '.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
