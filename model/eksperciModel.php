<?php

class eksperciModel extends Model {

	public $options = [ 'Table' => 'eksperci', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE . "eksperci/wszystkie");
			exit();
		}


		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_dodania, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE id = {$id} AND stat = '1' LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['eksperci'] = $sth->fetch( PDO::FETCH_ASSOC );

		// dodaję liczbę wyswietleń eksperta i ustawiam ciasteczko
		$cookie = ( $_COOKIE[COOKIES_EXPERT] ) ? unserialize( $_COOKIE[COOKIES_EXPERT] ) : [];
		if( $cookie ) {
			if( !in_array( $id , $cookie ) ) {
				$show = $this->data['eksperci']['wyswietlenia'] + 1;
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
				$sth->execute();
				array_push( $cookie , $id );
				$new_cookie = serialize( $cookie );
				setcookie( COOKIES_EXPERT, $new_cookie, time() + (60 * COOKIES_EXPERT_TIMEOUT), "/");
			}
		} else {
			$show = (int)$this->data['eksperci']['wyswietlenia'] + 1;
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
			$sth->execute();
			array_push( $cookie , $id );
			$new_cookie = serialize( $cookie );
			setcookie( COOKIES_EXPERT, $new_cookie, time() + (60 * COOKIES_EXPERT_TIMEOUT), "/");
		}

		$this->data['seo'] = [
			'title' => ' - ' . $this->data['eksperci']['imie'] . ' ' . $this->data['eksperci']['nazwisko'],
			'description' => $this->data['eksperci']['bio'],
			'keywords' => $this->data['dictionary'][48][LANG] . $this->data['eksperci']['imie'] . ' ' . $this->data['eksperci']['nazwisko'],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => ( @file_get_contents( BASE . 'userfiles/images/eksperci/' . $this->data['eksperci']['image'] ) ) ? BASE . 'userfiles/images/eksperci/' . $this->data['eksperci']['image'] : BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

	}

	function wszystkie() {
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND imie REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "' OR nazwisko REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['eksperci'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE stat = '1' {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

		$this->data['seo'] = [
			'title' => ' - ' . $this->data['dictionary'][56][LANG],
			'description' => $this->data['dictionary'][50][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . $this->data['dictionary'][56][LANG],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

	}

	function kategoria() {
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( $this->data['dictionary'][110][LANG], 1 );
		
		$sth = $this->pdo->prepare( "SELECT * FROM kategorie_n WHERE id = {$id} LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );
		$this->data['kategoria'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( !$this->data['kategoria'] )
			throw new modelException( "Kategoria nie istnieje", 1 );
			

		$sth = $this->pdo->prepare( "SELECT * FROM kategorie_newsroom WHERE id_kategorii = {$id}" );
		$sth->execute();
		$this->data['kat'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$ids = '';
		foreach( $this->data['kat'] as $aData ) {
			$ids .= $aData['id_artykulu'] . ',';
		}
		$ids = trim( $ids, ",;\n\r\t" );

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE id IN($ids)" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );
			
		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	function tag() {
		$this->data['tag'] = $tag = trim( strip_tags( $_GET['t'] ) );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' AND tagi REGEXP '{$tag}' ORDER BY pozycja" );

		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

}
