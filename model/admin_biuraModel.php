<?php

class admin_biuraModel extends Model {

	public $options = [ 'Table' => 'newsroom_klienci', 'Redirect' => 'admin_biura', 'SearchCol' => 'firma',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'firma';

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " WHERE {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';


		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} {$search} ORDER BY data_dodania DESC LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['biura'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} {$search} {$newsroom}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

	function wlacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie włączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z włączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function wylacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '0' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie wyłączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function usun() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$c = Routing::$routing['title'];

		if( $c != 'confirm' ) {
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT image, pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['pozycja'];

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE pozycja > $next" );
		$sth->execute();

		$sth = $this->pdo->prepare( "DELETE FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 ) {
			$_SESSION[I_SUCCESS] = "Poprawnie usunięto wpis";
			// @unlink( "userfiles/images/artykuly/" . $this->data['pos']['image'] );
		} else {
			$_SESSION[I_ERROR] = "Wystąpił problem z usunięciem wpisu";
		}

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function edytuj() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];
		
		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();


		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);
			
		$this->data['biuro'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( $_POST['edit'] == 1 ) {
			$this->data['firma'] = strip_tags( trim( $_POST['firma'] ) );
			$this->data['opis'] =  trim( $_POST['opis'] );
			$this->data['adres'] = trim( $_POST['ulica'] );
			$this->data['kod'] = trim( $_POST['kod'] );
			$this->data['miasto'] = trim( $_POST['miasto'] );
			$this->data['email'] = trim( $_POST['email'] );
			$this->data['www'] = trim( $_POST['www'] );
			$this->data['telefon'] = trim( $_POST['telefon'] );
			$this->data['kontakt_imie'] = trim( $_POST['kontakt_imie'] );
			$this->data['kontakt_nazwisko'] = trim( $_POST['kontakt_nazwisko'] );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			// tworzę nazwę pliku na bazie tytułu artykułu
			$filename = $id . '-' . Helper::uri_string( $this->data['tytul_pl'] );
			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET firma = :firma, opis = :opis, adres = :adres, kod = :kod, miasto = :miasto, email = :email, www = :www, telefon = :telefon, imie = :imie, nazwisko = :nazwisko, stat = '{$this->data['stat']}' WHERE id = {$id}" );
			
			if( !$sth->execute( 
				[
					':firma' => $this->data['firma'], 
					':opis' => $this->data['opis'], 
					':adres' => $this->data['adres'], 
					':kod' => $this->data['kod'],
					':miasto' => $this->data['miasto'],
					':email' => $this->data['email'],
					':www' => $this->data['www'],
					':telefon' => $this->data['telefon'],
					':imie' => $this->data['kontakt_imie'],
					':nazwisko' => $this->data['kontakt_nazwisko'],
				] ) 
			) {
				throw new modelException( "Wystąpił problem z edycją wpisu", 1);	
			} 

			$_SESSION[I_SUCCESS] = "Poprawnie zapisano zmiany";

			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}
	}

	function dodaj() {
		$this->getUser();
		$uid = $this->data['user_log']['id'];
		$id = (int)Routing::$routing['param'];

		if( $_POST['add'] == 1 ) {	
			$this->data['firma'] = strip_tags( trim( $_POST['firma'] ) );
			$this->data['opis'] =  trim( $_POST['opis'] );
			$this->data['adres'] = trim( $_POST['ulica'] );
			$this->data['kod'] = trim( $_POST['kod'] );
			$this->data['miasto'] = trim( $_POST['miasto'] );
			$this->data['email'] = trim( $_POST['email'] );
			$this->data['www'] = trim( $_POST['www'] );
			$this->data['telefon'] = trim( $_POST['telefon'] );
			$this->data['kontakt_imie'] = trim( $_POST['kontakt_imie'] );
			$this->data['kontakt_nazwisko'] = trim( $_POST['kontakt_nazwisko'] );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} (firma, opis, adres, kod, miasto, email, www, telefon, imie, nazwisko, stat ) VALUES( :firma, :opis, :adres, :kod, :miasto, :email, :www, :telefon, :imie, :nazwisko, '{$this->data['stat']}' )" );
			if( !$sth->execute( 
				[
					':firma' => $this->data['firma'], 
					':opis' => $this->data['opis'], 
					':adres' => $this->data['adres'], 
					':kod' => $this->data['kod'],
					':miasto' => $this->data['miasto'],
					':email' => $this->data['email'],
					':www' => $this->data['www'],
					':telefon' => $this->data['telefon'],
					':imie' => $this->data['kontakt_imie'],
					':nazwisko' => $this->data['kontakt_nazwisko'],
				] ) 
			) {
				throw new modelException( "Wystąpił problem z dodaniem wpisu", 1);	
			} 

			$_SESSION[I_SUCCESS] = "Poprawnie zapisano zmiany";

			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();

		}
	}

}
