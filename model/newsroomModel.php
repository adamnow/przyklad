<?php

class newsroomModel extends Model {

	public $options = [ 'Table' => 'newsroom', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE . "newsroom/wszystkie");
			exit();
		}


		// $sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_dodania, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE id = {$id} AND stat = '1' AND id_biura IS NOT NULL AND id_usera IS NOT NULL LIMIT 1" );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_dodania, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE id = {$id} AND stat = '1' LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['newsroom_art'] = $sth->fetch( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT * FROM firmy WHERE id = {$this->data['newsroom_art']['id_biura']} LIMIT 1" );
		$sth->execute();
		$this->data['biuro'] = $sth->fetch( PDO::FETCH_ASSOC );
		/*
		if( !$this->data['biuro'] || $this->data['biuro']['stat'] == 0 )
			$this->data['newsroom_art'] = null;
		*/

		if( $this->data['newsroom_art']['tagi'] ) {
			$this->data['tagi'] = explode( ',' , trim( $this->data['newsroom_art']['tagi'] ) );
			$this->data['tagi'] = array_filter( $this->data['tagi'] );
		}

		$sth = $this->pdo->prepare( "SELECT *, k.id as KID FROM kategorie_n as k JOIN kategorie_newsroom as ka ON(k.id = ka.id_kategorii) WHERE id_artykulu = {$id}" );
		$sth->execute();
		$this->data['kategorie'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// dodaję liczbę wyswietleń artykułu i ustawiam ciasteczko
		$cookie = ( $_COOKIE[COOKIES_NEWSROOM] ) ? unserialize( $_COOKIE[COOKIES_NEWSROOM] ) : [];
		if( $cookie ) {
			if( !in_array( $id , $cookie ) ) {
				$show = $this->data['newsroom_art']['wyswietlenia'] + 1;
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
				$sth->execute();
				array_push( $cookie , $id );
				$new_cookie = serialize( $cookie );
				setcookie( COOKIES_NEWSROOM, $new_cookie, time() + (60 * COOKIES_NEWSROOM_TIMEOUT), "/");
			}
		} else {
			$show = (int)$this->data['newsroom_art']['wyswietlenia'] + 1;
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
			$sth->execute();
			array_push( $cookie , $id );
			$new_cookie = serialize( $cookie );
			setcookie( COOKIES_NEWSROOM, $new_cookie, time() + (60 * COOKIES_NEWSROOM_TIMEOUT), "/");
		}

		// powiązane
		$this->data['kat'] = $this->data['tag'] = [];
		if( $this->data['newsroom_art']['tagi'] ) {
			$tagi_ = preg_replace( "/(;|,)/" , "|", $this->data['newsroom_art']['tagi'] );
			$tagi_ = trim( $tagi_, ",;|\n\t\r" );
			$tagi_ = str_replace( "| " , "|", $tagi_ );
			$sth = $this->pdo->prepare( "SELECT *, a.id as AID, DATE_FORMAT( a.data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} as a WHERE tagi REGEXP '{$tagi_}' AND id <> {$id}" );
			$sth->execute();
			$this->data['tag'] = $sth->fetchAll( PDO::FETCH_ASSOC );
		}


		if( $this->data['kategorie'] ) {
			foreach( $this->data['kategorie'] as $aData ) {
				$kategorie .= $aData['KID'] . '|';
			}
			$kategorie = trim( $kategorie, "|\n\t\r" );

			if( $tagi_ ) {
				$sth = $this->pdo->prepare( "SELECT *, a.id as AID, DATE_FORMAT( a.data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} as a JOIN kategorie_newsroom as ka ON(a.id = ka.id_artykulu) WHERE id_kategorii REGEXP '{$kategorie}' AND id_artykulu <> {$id} AND tagi NOT REGEXP '{$tagi_}'" );
			} else {
				$sth = $this->pdo->prepare( "SELECT *, a.id as AID, DATE_FORMAT( a.data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} as a JOIN kategorie_newsroom as ka ON(a.id = ka.id_artykulu) WHERE id_kategorii REGEXP '{$kategorie}' AND id_artykulu <> {$id}" );
			}
			$sth->execute();
			$this->data['kat'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		}

		$this->data['powiazane'] = array_merge( $this->data['tag'], $this->data['kat'] );
		shuffle( $this->data['powiazane'] );
	}

	function wszystkie() {
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		// $sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' AND id_biura IS NOT NULL AND id_usera IS NOT NULL {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' {$search} ORDER BY data_dodania DESC, pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['newsroom_art'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $this->data['newsroom_art'] as $k => &$aData ) {
			if( $aData['id_usera'] !== NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE id = {$aData['id_usera']} LIMIT 1" );
				$sth->execute();
				$aData['user'] = $sth->fetch( PDO::FETCH_ASSOC );

				$sth = $this->pdo->prepare( "SELECT * FROM firmy WHERE id = {$aData['id_biura']} AND stat = '1' LIMIT 1" );
				$sth->execute();
				$aData['biuro'] = $sth->fetch( PDO::FETCH_ASSOC );
				/*
				if( $aData['user']['stat'] == 0 || $aData['biuro']['stat'] == 0 ) {
					unset( $this->data['newsroom_art'][$k] );
				}
				*/
			}
		}


		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE stat = '1' AND id_biura IS NOT NULL AND id_usera IS NOT NULL {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];
	}

	function kategoria() {
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( $this->data['dictionary'][110][LANG], 1 );
		
		$sth = $this->pdo->prepare( "SELECT * FROM kategorie_n WHERE id = {$id} LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );
		$this->data['kategoria'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( !$this->data['kategoria'] )
			throw new modelException( $this->data['dictionary'][111][LANG], 1 );
			

		$sth = $this->pdo->prepare( "SELECT * FROM kategorie_newsroom WHERE id_kategorii = {$id}" );
		$sth->execute();
		$this->data['kat'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$ids = '';
		foreach( $this->data['kat'] as $aData ) {
			$ids .= $aData['id_artykulu'] . ',';
		}
		$ids = trim( $ids, ",;\n\r\t" );

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE id IN($ids)" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );
			
		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

	function tag() {
		$this->data['tag'] = $tag = trim( strip_tags( $_GET['t'] ) );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' AND tagi REGEXP '{$tag}' ORDER BY pozycja" );

		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['newsroom_art'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

}
