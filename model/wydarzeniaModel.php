<?php

class wydarzeniaModel extends Model {

	public $options = [ 'Table' => 'wydarzenia', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE . "wydarzenia/wszystkie");
			exit();
		}


		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_wydarzenia, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE id = {$id} AND stat = '1' LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['wydarzenia'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( $this->data['wydarzenia']['tagi'] ) {
			$this->data['tagi'] = explode( ',' , trim( $this->data['wydarzenia']['tagi'] ) );
			$this->data['tagi'] = array_filter( $this->data['tagi'] );
		}

		$sth = $this->pdo->prepare( "SELECT * FROM kategorie_w WHERE id = {$this->data['wydarzenia']['id_kategorii']} LIMIT 1" );
		$sth->execute();
		$this->data['kategorie'] = $sth->fetch( PDO::FETCH_ASSOC );

		// dodaję liczbę wyswietleń artykułu i ustawiam ciasteczko
		/*
		$cookie = ( $_COOKIE[COOKIES_NEWSROOM] ) ? unserialize( $_COOKIE[COOKIES_NEWSROOM] ) : [];
		if( $cookie ) {
			if( !in_array( $id , $cookie ) ) {
				$show = $this->data['newsroom_art']['wyswietlenia'] + 1;
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
				$sth->execute();
				array_push( $cookie , $id );
				$new_cookie = serialize( $cookie );
				setcookie( COOKIES_NEWSROOM, $new_cookie, time() + (60 * COOKIES_NEWSROOM_TIMEOUT), "/");
			}
		} else {
			$show = (int)$this->data['newsroom_art']['wyswietlenia'] + 1;
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
			$sth->execute();
			array_push( $cookie , $id );
			$new_cookie = serialize( $cookie );
			setcookie( COOKIES_NEWSROOM, $new_cookie, time() + (60 * COOKIES_NEWSROOM_TIMEOUT), "/");
		}
		*/
		
		$this->data['seo'] = [
			'title' => ' - ' . html_entity_decode( $this->data['wydarzenia']['tytul_' . LANG] ),
			'description' => html_entity_decode( strip_tags( $this->data['wydarzenia']['zajawka_' . LANG] ) ),
			'keywords' => html_entity_decode( strip_tags( $this->data['wydarzenia']['tagi'] ) ) . ',' . $this->data['dictionary'][48][LANG],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => ( @file_get_contents( BASE . 'userfiles/images/wydarzenia/' . $this->data['wydarzenia']['image'] ) ) ? BASE . 'userfiles/images/wydarzenia/' . $this->data['wydarzenia']['image'] : BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

	}

	function wszystkie() {
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$f1 = ( $_GET['f']['cat'] ) ? " AND id_kategorii = " . (int)$_GET['f']['cat'] : "";
		$f2 = ( $_GET['f']['m'] ) ? " AND DATE_FORMAT( data_wydarzenia, '%m' ) = " . (int)$_GET['f']['m'] : "";
		$f3 = ( $_GET['f']['y'] ) ? " AND DATE_FORMAT( data_wydarzenia, '%Y' ) = " . (int)$_GET['f']['y'] : "";

		$filter = $f1 . $f2 . $f3;

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_wydarzenia, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE stat = '1' {$search} {$filter} ORDER BY data_dodania DESC LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['wydarzenia'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		// rok start 
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_wydarzenia, '%Y' ) as rok FROM wydarzenia WHERE stat = '1' ORDER BY DATE_FORMAT( data_wydarzenia, '%Y' ) LIMIT 1" );
		$sth->execute();
		$this->data['rok'] = $sth->fetch( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE stat = '1' {$search} {$filter}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

		$this->data['seo'] = [
			'title' => ' - ' . $this->data['dictionary'][53][LANG],
			'description' => $this->data['dictionary'][50][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . ' kalendarium, ' . $this->data['dictionary'][53][LANG],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];
	}

	function tag() {
		$this->data['tag'] = $tag = trim( strip_tags( $_GET['t'] ) );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_wydarzenia, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' AND tagi REGEXP '{$tag}' ORDER BY pozycja" );

		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['wydarzenia'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

}
