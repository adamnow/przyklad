<?php

class galeriaModel extends Model {

	public $options = [ 'Table' => 'galeria', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE . "galeria/kategorie");
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT * FROM galeria WHERE id = {$id} AND stat = '1' LIMIT 1" );
		$sth->execute();
		$this->data['galeria'] = $sth->fetch( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_dodania, '%d.%m.%Y') as data FROM zdjecia WHERE id_galerii = {$id} AND stat = '1' ORDER BY pozycja" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['zdjecia'] = $sth->fetchAll( PDO::FETCH_ASSOC );

	}


	function kategorie() {

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );
			
		$this->data['galerie'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE stat = '1' {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];
	}

	function tag() {
		$this->data['tag'] = $tag = trim( strip_tags( $_GET['t'] ) );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' AND tagi REGEXP '{$tag}' ORDER BY pozycja" );

		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );
	}

}
