<?php
use \FreshMail\RestApi;
class admin_freshmailModel extends Model {

	public $options = [ 'Table' => 'users', 'Redirect' => 'admin_freshmail', 'SearchCol' => '',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];
		$id = Routing::$routing['param'];


	}

	function dodaj() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];
		$id = Routing::$routing['param'];

		$sth = $this->pdo->prepare( "SELECT *, '1' as czytelnik, '0' as newsletter, '0' as prenumerator FROM czytelnicy WHERE email <> '' AND freshmail = '0' ORDER BY id" );
		$sth->execute();
		$this->data['users'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT *, '0' as czytelnik, '1' as newsletter, '0' as prenumerator FROM newsletter WHERE email <> '' AND freshmail = '0' ORDER BY id" );
		$sth->execute();
		$this->data['newsletter'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT *, '0' as czytelnik, '0' as newsletter, '1' as prenumerator FROM prenumerata WHERE email <> '' AND freshmail = '0' ORDER BY id" );
		$sth->execute();
		$this->data['prenumerata'] = $sth->fetchAll( PDO::FETCH_ASSOC );
		// var_dump( $this->data['users'] );

		$results = array_merge( $this->data['users'], $this->data['newsletter'] );
		$results = array_merge( $results, $this->data['prenumerata'] );

		if( !$results ) {
			$_SESSION[I_ERROR] = 'Brak nowych adresów email do wyeksportowania';
			header("Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		foreach( $results as $k => &$item ) {
			$this->rest = new RestApi();
			$this->rest->setApiKey(FM_API);
			$this->rest->setApiSecret(FM_API_SECRET);

			$kod = $miejscowosc = '';

			if( array_key_exists( 'kod_miejscowosc' , $item ) ) {
				$m = explode( ' ' , $item['kod_miejscowosc'] );
				$kod = trim( $m[0] );
				$miejscowosc = trim( $m[1] );
			}

			$data = [
			    'email' => $item['email'],
			    'list'  => FM_API_LIST,

			    'custom_fields' => [
			        'imie' => ( array_key_exists( 'imie' , $item ) ) ? $item['imie'] : '',
			        'nazwisko'  => ( array_key_exists( 'nazwisko' , $item ) ) ? $item['nazwisko'] : '',
			        'firma'  => ( array_key_exists( 'firma' , $item ) ) ? $item['firma'] : '',
			        'adres_firmy'  => ( array_key_exists( 'adres' , $item ) ) ? $item['adres'] : '',
			        'kod'  => preg_replace( "/;,\s/" , '', $kod ),
			        'miejscowosc'  => preg_replace( "/;,\s/" , '', $miejscowosc ),
			        'nip'  => ( array_key_exists( 'nip' , $item ) ) ? $item['nip'] : '',
			        'telefon'  => ( array_key_exists( 'telefon' , $item ) ) ? $item['telefon'] : '',
			        'stanowisko'  => ( array_key_exists( 'stanowisko' , $item ) ) ? $item['stanowisko'] : '',
			        'prenumerata_typ'  => ( array_key_exists( 'prenumerata' , $item ) ) ? $item['prenumerata'] : '',
			        'prenumerata_okres'  => ( array_key_exists( 'okres' , $item ) ) ? $item['okres'] : '',
			        'prenumerata'  => ( array_key_exists( 'prenumerator' , $item ) ) ? $item['prenumerator'] : '0',
			        'dostep_online'  => ( array_key_exists( 'czytelnik' , $item ) ) ? $item['czytelnik'] : '0',
			        'newsletter'  => ( array_key_exists( 'newsletter' , $item ) ) ? $item['newsletter'] : '0',
			        'data_dodania'  => ( array_key_exists( 'data_dodania' , $item ) ) ? $item['data_dodania'] : $item['data_zapisu'],
			        'stat_cms'  => $item['stat'],
			    ],

			    'state'   => 1,
			    'confirm' => 0,
			];


			try {
			    $response = (array)$this->rest->doRequest('subscriber/add', $data);
			    if( $response['status'] == 'OK' ) {
					if( $item['prenumerator'] == 1 ) {
						$sth = $this->pdo->prepare( "UPDATE prenumerata SET freshmail = '1' WHERE id = {$item['id']}" );
						$sth->execute();
					}

					if( $item['newsletter'] == 1 ) {
						$sth = $this->pdo->prepare( "UPDATE newsletter SET freshmail = '1' WHERE id = {$item['id']}" );
						$sth->execute();
					}

					if( $item['czytelnik'] == 1 ) {
						$sth = $this->pdo->prepare( "UPDATE czytelnicy SET freshmail = '1' WHERE id = {$item['id']}" );
						$sth->execute();
					}
			    }
			} catch ( Exception $e ) {
				if( $e->getCode() == 1304 ) {
					if( $item['prenumerator'] == 1 ) 
						$col = 'prenumerata';
					elseif( $item['czytelnik'] == 1 ) 
						$col = 'dostep_online';
					elseif( $item['newsletter'] == 1 ) 
						$col = 'newsletter';

					$dataEdit = [
					    'email' => $item['email'],
					    'list'  => FM_API_LIST,
					    'custom_fields' => [
					        $col => 1,
					    ],
					];
					try {
				    	$response = (array)$this->rest->doRequest('subscriber/edit', $dataEdit);
				    	if( $response['status'] == 'OK' ) {
							if( $item['prenumerator'] == 1 ) {
								$sth = $this->pdo->prepare( "UPDATE prenumerata SET freshmail = '1' WHERE id = {$item['id']}" );
								$sth->execute();
							}

							if( $item['newsletter'] == 1 ) {
								$sth = $this->pdo->prepare( "UPDATE newsletter SET freshmail = '1' WHERE id = {$item['id']}" );
								$sth->execute();
							}

							if( $item['czytelnik'] == 1 ) {
								$sth = $this->pdo->prepare( "UPDATE czytelnicy SET freshmail = '1' WHERE id = {$item['id']}" );
								$sth->execute();
							}
					    }
					} catch ( Exception $subE ) {
						$aa++;
						$_SESSION[I_ERROR] = "Wystąpił proble z eksportem {$aa} adresów email";
					}
				    unset( $dataEdit );
				} 

				if( $e->getCode() == 1301 ) {
					$a++;
					$_SESSION[I_ERROR] = "{$a} pozycja(i) nie zostało wyeksportowanych do bazy Freshmail. Adresy email mają niepoprawny format.";
				}

			    // echo 'Error message: ' . $e->getMessage() . ', Error code: ' . $e->getCode() . ', HTTP code: ' . $this->rest->getHttpCode() . PHP_EOL;
			}
		}

		if( !$_SESSION[I_ERROR] )
			$_SESSION[I_SUCCESS] = "Poprawnie wyeksportowano adresy email";

		header("Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

}
