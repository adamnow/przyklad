<?php

class czasopismoModel extends Model {

	public $options = [ 'Table' => 'czasopismo', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		if( $_POST['send-btn'] == 1 ) {
			$this->data['firma'] = strip_tags( trim( $_POST['firma'] ) );
			$this->data['adres'] = strip_tags( trim( $_POST['adres'] ) );
			$this->data['miejscowosc'] = strip_tags( trim( $_POST['miejscowosc'] ) );
			$this->data['nip'] = strip_tags( trim( $_POST['nip'] ) );
			$this->data['email'] = strip_tags( trim( $_POST['email'] ) );
			$this->data['telefon'] = strip_tags( trim( $_POST['telefon'] ) );
			$this->data['zgoda'] = ( $_POST['zgoda'] == 1 ) ? 1 : 0;
			$this->data['inny_adres'] = ( $_POST['inny_adres'] == 1 ) ? 1 : 0;
			$this->data['prenumerata'] = strip_tags( trim( $_POST['prenumerata'] ) );
			$this->data['okres'] = strip_tags( trim( $_POST['okres'] ) );
			$this->data['liczba'] = (int)$_POST['count'];

			$this->data['wysylka_dane'] = ( $_POST['inny_adres'] == 1 ) ? strip_tags( trim( $_POST['wysylka_dane'] ) ) : $this->data['firma'];
			$this->data['wysylka_adres'] = ( $_POST['inny_adres'] == 1 ) ? strip_tags( trim( $_POST['wysylka_adres'] ) ) : $this->data['adres'];
			$this->data['wysylka_miejscowosc'] = ( $_POST['inny_adres'] == 1 ) ? strip_tags( trim( $_POST['wysylka_miejscowosc'] ) ) : $this->data['miejscowosc'];

			$sth = $this->pdo->prepare( "INSERT INTO prenumerata (firma, adres, kod_miejscowosc, nip, email, telefon, inny_adres, wysylka_firma, wysylka_adres, wysylka_kod_miejscowosc, zgoda, prenumerata, okres, egzemplarze, nowe, stat) VALUES(:firma, :adres, :kod_miejscowosc, :nip, :email, :telefon, '{$this->data[inny_adres]}', :wysylka_firma, :wysylka_adres, :wysylka_kod_miejscowosc, '{$this->data[zgoda]}', :prenumerata, :okres, :egzemplarze, '1', '0')" );


			$sth->execute(
					[
						':firma' => $this->data['firma'],
						':adres' => $this->data['adres'],
						':kod_miejscowosc' => $this->data['miejscowosc'],
						':nip' => $this->data['nip'],
						':email' => $this->data['email'],
						':telefon' => $this->data['telefon'],
						':wysylka_firma' => $this->data['wysylka_dane'],
						':wysylka_adres' => $this->data['wysylka_adres'],
						':wysylka_kod_miejscowosc' => $this->data['wysylka_miejscowosc'],
						':prenumerata' => $this->data['prenumerata'],
						':okres' => $this->data['okres'],
						':egzemplarze' => $this->data['liczba'],
					]
				);

			if( $sth->rowCount() < 1 )
				throw new modelException( $this->data['dictionary'][120][LANG], 1 );

			$lid = $this->pdo->lastInsertId();

			$sth = $this->pdo->prepare( "SELECT * FROM czytelnicy WHERE email = :email LIMIT 1" );
			$sth->execute( [':email' => $this->data['email']] );

			if( $sth->rowCount() < 1 ) {
				$token = sha1( $this->data['firma'] . $lid . $this->data['email'] );
				$sth = $this->pdo->prepare( "INSERT INTO czytelnicy (email, firma, token, stat) VALUES (:email, :firma, '{$token}', '1')" );
				$sth->execute(
						[
							':email' => $this->data['email'],
							':firma' => $this->data['firma'],
						]
					);
			}


			$_SESSION[I_SUCCESS] = $this->data['dictionary'][121][LANG];
			header( "Location: " . BASE . "czasopismo" );
			exit();
		}

		$this->data['seo'] = [
			'title' => ' - ' . $this->data['dictionary'][54][LANG],
			'description' => $this->data['dictionary'][55][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . ' kalendarium, ' . $this->data['dictionary'][54][LANG],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

	}

}
