<?php

class admin_prenumerataModel extends Model {

	public $options = [ 'Table' => 'prenumerata', 'Redirect' => 'admin_prenumerata', 'SearchCol' => 'tytul_pl',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " WHERE nip = '{$this->data['search']}' OR firma REGEXP '" . $this->data['search'] . "' OR email REGEXP '" . $this->data['search'] .  "'"  : '';

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} {$search} ORDER BY data_dodania DESC LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['prenumerata_'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

	function nowe() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND nip = '{$this->data['search']}' OR firma REGEXP '" . $this->data['search'] . "' OR email REGEXP '" . $this->data['search'] .  "'"  : '';

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE nowe = '1' {$search} ORDER BY data_dodania DESC LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['prenumerata_'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

	function wlacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie włączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z włączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function wylacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '0' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie wyłączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function usun() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$c = Routing::$routing['title'];

		if( $c != 'confirm' ) {
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT image, pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['pozycja'];

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE pozycja > $next" );
		$sth->execute();

		$sth = $this->pdo->prepare( "DELETE FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 ) {
			$_SESSION[I_SUCCESS] = "Poprawnie usunięto wpis";
			@unlink( "userfiles/images/online/" . $this->data['pos']['image'] );
		} else {
			$_SESSION[I_ERROR] = "Wystąpił problem z usunięciem wpisu";
		}

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function edytuj() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );
			
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);
			
		$this->data['prenumerata'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( $_POST['edit'] == 1 ) {
			$this->data['prenumerata'] = strip_tags( trim( $_POST['prenumerata'] ) );
			$this->data['okres'] = strip_tags( trim( $_POST['okres'] ) );
			$this->data['ilosc'] = (int)$_POST['ilosc'];
			$this->data['firma'] = strip_tags( trim( $_POST['firma'] ) );
			$this->data['adres'] = strip_tags( trim( $_POST['adres'] ) );
			$this->data['kod_miejscowosc'] = trim( $_POST['miejscowosc'] );
			$this->data['nip'] = trim( $_POST['nip'] );
			$this->data['email'] = trim( $_POST['email'] );
			$this->data['telefon'] = trim( $_POST['telefon'] );

			
			$this->data['firma_inny'] = strip_tags( trim( $_POST['firma_inny'] ) );
			$this->data['adres_inny'] = strip_tags( trim( $_POST['adres_inny'] ) );
			$this->data['miejscowosc_inny'] = strip_tags( trim( $_POST['miejscowosc_inny'] ) );

			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET firma = :firma, adres = :adres, kod_miejscowosc = :kod_miejscowosc, nip = :nip, email = :email, telefon = :telefon, wysylka_firma = :wysylka_firma, wysylka_adres = :wysylka_adres, wysylka_kod_miejscowosc = :wysylka_kod_miejscowosc, prenumerata = :prenumerata, okres = :okres, egzemplarze = '{$this->data[ilosc]}' WHERE id = {$id}" );
			
			if( !$sth->execute( 
				[
					':firma' => $this->data['firma'],
					':adres' => $this->data['adres'],
					':kod_miejscowosc' => $this->data['kod_miejscowosc'],
					':nip' => $this->data['nip'],
					':email' => $this->data['email'],
					':telefon' => $this->data['telefon'],
					':wysylka_firma' => $this->data['firma_inny'],
					':wysylka_adres' => $this->data['adres_inny'],
					':wysylka_kod_miejscowosc' => $this->data['miejscowosc_inny'],
					':prenumerata' => $this->data['prenumerata'],
					':okres' => $this->data['okres'],
				] ) 
				) {

				throw new modelException( "Wystąpił problem z edycją wpisu", 1 );
				
			}
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono wpis";
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}
	}

	function dodaj() {
		$this->getUser();
		$uid = $this->data['user_log']['id'];

		if( $_POST['add'] == 1 ) {	
			$this->data['prenumerata'] = strip_tags( trim( $_POST['prenumerata'] ) );
			$this->data['okres'] = strip_tags( trim( $_POST['okres'] ) );
			$this->data['ilosc'] = (int)$_POST['ilosc'];
			$this->data['firma'] = strip_tags( trim( $_POST['firma'] ) );
			$this->data['adres'] = strip_tags( trim( $_POST['adres'] ) );
			$this->data['kod_miejscowosc'] = trim( $_POST['miejscowosc'] );
			$this->data['nip'] = trim( $_POST['nip'] );
			$this->data['email'] = trim( $_POST['email'] );
			$this->data['telefon'] = trim( $_POST['telefon'] );
			$this->data['inny'] = ( $_POST['inny'] == 1 ) ? 1 : 0;

			
			$this->data['firma_inny'] = ( $this->data['inny'] == 1 ) ? strip_tags( trim( $_POST['firma_inny'] ) ) : $this->data['firma'];
			$this->data['adres_inny'] = ( $this->data['inny'] == 1 ) ? strip_tags( trim( $_POST['adres_inny'] ) ) : $this->data['adres'];
			$this->data['miejscowosc_inny'] = ( $this->data['inny'] == 1 ) ? strip_tags( trim( $_POST['miejscowosc_inny'] ) ) : $this->data['kod_miejscowosc'];


			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} ( firma, adres, kod_miejscowosc, nip, email, telefon, inny_adres, wysylka_firma, wysylka_adres, wysylka_kod_miejscowosc, zgoda, prenumerata, okres, egzemplarze, data_dodania, nowe, stat ) VALUES(:firma, :adres, :kod_miejscowosc, :nip, :email, :telefon, '{$this->data[inny]}', :wysylka_firma, :wysylka_adres, :wysylka_kod_miejscowosc, '1', :prenumerata, :okres, '{$this->data[ilosc]}', NOW(), '0', '1')" );
			if( !$sth->execute(
					[
						':firma' => $this->data['firma'],
						':adres' => $this->data['adres'],
						':kod_miejscowosc' => $this->data['kod_miejscowosc'],
						':nip' => $this->data['nip'],
						':email' => $this->data['email'],
						':telefon' => $this->data['telefon'],
						':wysylka_firma' => $this->data['firma_inny'],
						':wysylka_adres' => $this->data['adres_inny'],
						':wysylka_kod_miejscowosc' => $this->data['miejscowosc_inny'],
						':prenumerata' => $this->data['prenumerata'],
						':okres' => $this->data['okres'],
					]
				)) {
				throw new modelException( "Wystąpił problem z dodaniem prenumeraty", 1 );
				
			}
			$_SESSION[I_SUCCESS] = "Poprawnie dodano wpis";
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();

		}
	}

	function potwierdz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		// $target = Routing::$routing['title'];
		$target = 'widok';

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = {$id} AND stat = '0' LIMIT 1" );
		$sth->execute();
		$this->data['zamowienie'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( !$this->data['zamowienie'] ) {
			$_SESSION[I_ERROR] = "Zamówienie nie istnieje lub zostało już potwierdzone";
			header( "Location: " . BASE . $this->options['Redirect'] . '/' . $target );
			exit();
		}
			

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET nowe = '0', stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 ) {
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono status zamówienia. Do zamawiającego została wysłana wiadomosc email.";
			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPDebug = 0;
			$mail->SMTPAuth = true;
			$mail->addReplyTo( MAIL_FROM, MAIL_FROM_NAME );
			$mail->addAddress( $this->data['zamowienie']['email'], '');
			$mail->Subject = $this->data['dictionary'][46][LANG];

			$body = @file_get_contents( 'public/mail/mail.html' );
			$body = preg_replace( "/{{BASE}}/" , BASE, $body );
			$body = preg_replace( "/{{CONTENT}}/" , $this->data['dictionary'][47][LANG], $body );
			if( $cta )
				$body = preg_replace( "/{{CTA}}/" , $cta, $body );
			else
				$body = preg_replace( "/{{CTA}}/" , "", $body );
			
			$body = preg_replace( "/{{FB}}/" , $this->data['dictionary'][22][LANG], $body );

			if( Routing::$routing['param'] == 'strona' )
				$link = "<a href='" . BASE . "artykul/dodaj_konto/strona/" . Routing::$routing[title] . "/?token={$token}'>LINK WERYFIKACYJNY</a>";
			else
				$link = "<a href='" . BASE . "artykul/dodaj_konto/{$id}/?token={$token}'>LINK WERYFIKACYJNY</a>";
			$body = preg_replace( "/{{LINK}}/" , $link, $body );

			$mail->msgHTML( $body );
			if (!$mail->send())
			    throw new modelException( "Wystąpił problem z wysłaniem emaila", 4016 );

		}
		else {
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";
		}

		header( "Location: " . BASE . $this->options['Redirect'] . '/' . $target );
		exit();
	}

	function xls() {
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} ORDER BY email" );
		$sth->execute();
		$this->data['u'] = $sth->fetchAll( PDO::FETCH_ASSOC );


		$objPHPExcel = new PHPExcel();
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', 'firma')
		            ->setCellValue('B1', 'adres')
		            ->setCellValue('C1', 'kod, miejscowość')
		            ->setCellValue('D1', 'nip')
		            ->setCellValue('E1', 'email')
		            ->setCellValue('F1', 'telefon')
		            ->setCellValue('G1', 'wysyłka nazwa')
		            ->setCellValue('H1', 'wysyłka adres')
		            ->setCellValue('I1', 'wysyłka kod, miejscowość')
		            ->setCellValue('J1', 'prenumerata')
		            ->setCellValue('K1', 'okres')
		            ->setCellValue('L1', 'egzemplarze')
		            ->setCellValue('M1', 'data dodania')
		            ->setCellValue('N1', 'nowe zgłoszenie')
		            ->setCellValue('O1', 'status');

		$objPHPExcel->getActiveSheet()->getStyle("A1:O1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

		$row = 1;
		foreach( $this->data['u'] as $item ) {
			$row++;
			$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValueExplicit('A' . $row , $item['firma'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('B' . $row , $item['adres'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('C' . $row , $item['kod_miejscowosc'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('D' . $row , $item['nip'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('E' . $row , $item['email'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('F' . $row , $item['telefon'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('G' . $row , $item['wysylka_firma'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('H' . $row , $item['wysylka_adres'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('I' . $row , $item['wysylka_kod_miejscowosc'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('J' . $row , $item['prenumerata'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('K' . $row , $item['okres'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('L' . $row , $item['egzemplarze'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('M' . $row , $item['data_dodania'], PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('N' . $row , $n = ( $item['nowe'] == 1 ) ? 'TAK' : 'NIE', PHPExcel_Cell_DataType::TYPE_STRING )
			            ->setCellValueExplicit('O' . $row, $s = ( $item['stat'] == 1 ) ? 'zrealizowane' : 'niezrealizowane', PHPExcel_Cell_DataType::TYPE_STRING );
		}

		$objPHPExcel->getActiveSheet()->setTitle( 'prenumeraty-' . date('Ymd') );
		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="prenumeraty-' . date( 'Ymd' ) . '.xls"');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header ('Cache-Control: cache, must-revalidate');
		header ('Pragma: public');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
