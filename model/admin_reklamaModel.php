<?php

class admin_reklamaModel extends Model {

	public $options = [ 'Table' => 'reklama', 'Redirect' => 'admin_reklama', 'SearchCol' => 'tytul_pl',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}
/**
 * [widok boczne reklamy]
 * @return [type] [description]
 */
	function widok() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$sth = $this->pdo->prepare( "SELECT *, IF( ( NOW() >= data_start AND NOW() <= data_stop ), '1', '0' ) as _ACT_ FROM {$this->options['Table']} WHERE top ='0' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['reklama'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $this->data['reklama'] as &$aData ) {
			if( $aData['id_usera'] !== NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE id = {$aData['id_usera']} LIMIT 1" );
				$sth->execute();
				$aData['user'] = $sth->fetch( PDO::FETCH_ASSOC );
			}
		}


		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE top ='0' {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

/**
 * [widok_top górne reklamy]
 * @return [type] [description]
 */
	function widok_top() {
		$this->getUser();
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		$sth = $this->pdo->prepare( "SELECT *, IF( ( NOW() >= data_start AND NOW() <= data_stop ), '1', '0' ) as _ACT_ FROM {$this->options['Table']} WHERE top ='1' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['reklama'] = $sth->fetchAll( PDO::FETCH_ASSOC );


		foreach ( $this->data['reklama'] as &$aData ) {
			if( $aData['id_usera'] !== NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE id = {$aData['id_usera']} LIMIT 1" );
				$sth->execute();
				$aData['user'] = $sth->fetch( PDO::FETCH_ASSOC );
			}
		}


		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE top ='1' {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];
	}

/**
 * [wlacz włącz reklamę]
 * @return [type] [description]
 */
	function wlacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie włączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z włączeniem wpisu";

		if( Routing::$routing['title'] == 'top' )
			header( "Location: " . BASE . "admin_reklama/widok_top" );
		else
			header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
/**
 * [wylacz wyłącz reklamę]
 * @return [type] [description]
 */
	function wylacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '0' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie wyłączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";
		if( Routing::$routing['title'] == 'top' )
			header( "Location: " . BASE . "admin_reklama/widok_top" );
		else
			header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
/**
 * [usun usuń reklamę]
 * @return [type] [description]
 */
	function usun() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$c = Routing::$routing['title'];

		if( $c != 'confirm' ) {
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT image, pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['pozycja'];

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE pozycja > $next" );
		$sth->execute();

		$sth = $this->pdo->prepare( "DELETE FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 ) {
			$_SESSION[I_SUCCESS] = "Poprawnie usunięto wpis";
			@unlink( "userfiles/images/rekl/" . $this->data['pos']['image'] );
		} else {
			$_SESSION[I_ERROR] = "Wystąpił problem z usunięciem wpisu";
		}

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

/**
 * [edytuj edytuj reklamę]
 * @return [type] [description]
 */
	function edytuj() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );
			
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		$this->data['reklama'] = $sth->fetch( PDO::FETCH_ASSOC );

		$date_start = explode( ' ' , $this->data['reklama']['data_start'] );
		$date_stop = explode( ' ' , $this->data['reklama']['data_stop'] );


		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);

		$this->data['reklama']['data_start'] = $date_start[0];
		$this->data['reklama']['data_stop'] = $date_stop[0];

		$hstart = explode( ':' , $date_start[1] );
		$hstop = explode( ':' , $date_stop[1] );

		$this->data['reklama']['godzina_start'] = $hstart[0] . ':' . $hstart[1];
		$this->data['reklama']['godzina_stop'] = $hstop[0] . ':' . $hstop[1];

		if( $_POST['edit'] == 1 ) {
			$this->data['tytul_pl'] = strip_tags( trim( $_POST['tytul_pl'] ) );
			$this->data['link'] = strip_tags( trim( $_POST['link'] ) );
			$this->data['data_start'] = strip_tags( trim( $_POST['data_start'] ) );
			$this->data['godzina_start'] = strip_tags( trim( $_POST['godzina_start'] ) );
			$this->data['data_stop'] = strip_tags( trim( $_POST['data_stop'] ) );
			$this->data['godzina_stop'] = strip_tags( trim( $_POST['godzina_stop'] ) );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			$data_start = $this->data['data_start'] . ' ' . $this->data['godzina_start'] . ':00';
			$data_stop = $this->data['data_stop'] . ' ' . $this->data['godzina_stop'] . ':00';

			// tworzę nazwę pliku na bazie tytułu artykułu
			$filename = $id . '-' . Helper::uri_string( $this->data['tytul_pl'] );
			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET tytul_pl = :tytul_pl, link = :link, data_start = :datastart, data_stop = :datastop, stat = '{$this->data[stat]}' WHERE id = {$id}" );
			
			if( $sth->execute( 
				[
					':tytul_pl' => $this->data['tytul_pl'], 
					':link' => $this->data['link'], 
					':datastart' => $data_start, 
					':datastop' => $data_stop, 
				] ) 
				) {

				// upload pliku
				if( $_FILES['image_upload']['tmp_name'] ) {
					@unlink( 'userfiles/images/rekl/' . $this->data['reklama']['image'] );
					$extension = end( explode( '.' , $_FILES['image_upload']['name'] ) );
					$optionImage = [
						'index' => 'image_upload',
						'min-width' => true,
						'height-auto' => true,
						'width' => 300,
						'height' => 600,
						'tmp_dir' => 'userfiles/_tmpfile/',
						'dir' => 'userfiles/images/rekl/',
						'filename' => $filename,
						'extension' => $extension,
					];
					Helper::uploadImage( $optionImage );
					// update nazwy pliku artykułu
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}.{$extension}' WHERE id = {$id}" );
					$sth->execute();
				} else {
					$img = explode( "." , $this->data['reklama']['image'] );
					@rename( 'userfiles/images/rekl/' . $this->data['reklama']['image'], 'userfiles/images/rekl/' . $filename . '.' . end( $img ) );
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}." . end( $img ) . "' WHERE id = {$id}" );
					$sth->execute();
				}

				if( !$_SESSION[I_ERROR] && !$_SESSION[I_INFO] )
					$_SESSION[I_SUCCESS] = "Poprawnie edytowano wpis";
			} else {
				$_SESSION[I_ERROR] = "Nie wprowadzono zmian we wpisie";
			}

			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}
	}

/**
 * [edytuj edytuj górną reklamę]
 * @return [type] [description]
 */
	function edytuj_top() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );
			
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		$this->data['reklama'] = $sth->fetch( PDO::FETCH_ASSOC );

		$date_start = explode( ' ' , $this->data['reklama']['data_start'] );
		$date_stop = explode( ' ' , $this->data['reklama']['data_stop'] );


		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);

		$this->data['reklama']['data_start'] = $date_start[0];
		$this->data['reklama']['data_stop'] = $date_stop[0];

		$hstart = explode( ':' , $date_start[1] );
		$hstop = explode( ':' , $date_stop[1] );

		$this->data['reklama']['godzina_start'] = $hstart[0] . ':' . $hstart[1];
		$this->data['reklama']['godzina_stop'] = $hstop[0] . ':' . $hstop[1];

		if( $_POST['edit'] == 1 ) {
			$this->data['tytul_pl'] = strip_tags( trim( $_POST['tytul_pl'] ) );
			$this->data['link'] = strip_tags( trim( $_POST['link'] ) );
			$this->data['data_start'] = strip_tags( trim( $_POST['data_start'] ) );
			$this->data['godzina_start'] = strip_tags( trim( $_POST['godzina_start'] ) );
			$this->data['data_stop'] = strip_tags( trim( $_POST['data_stop'] ) );
			$this->data['godzina_stop'] = strip_tags( trim( $_POST['godzina_stop'] ) );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			$data_start = $this->data['data_start'] . ' ' . $this->data['godzina_start'] . ':00';
			$data_stop = $this->data['data_stop'] . ' ' . $this->data['godzina_stop'] . ':00';

			// tworzę nazwę pliku na bazie tytułu artykułu
			$filename = $id . '-' . Helper::uri_string( $this->data['tytul_pl'] );
			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET tytul_pl = :tytul_pl, link = :link, data_start = :datastart, data_stop = :datastop, stat = '{$this->data[stat]}' WHERE id = {$id}" );
			
			if( $sth->execute( 
				[
					':tytul_pl' => $this->data['tytul_pl'], 
					':link' => $this->data['link'], 
					':datastart' => $data_start, 
					':datastop' => $data_stop, 
				] ) 
				) {

				// upload pliku
				if( $_FILES['image_upload']['tmp_name'] ) {
					@unlink( 'userfiles/images/rekl/' . $this->data['reklama']['image'] );
					$extension = end( explode( '.' , $_FILES['image_upload']['name'] ) );
					$optionImage = [
						'index' => 'image_upload',
						// 'min-width' => true,
						// 'height-auto' => true,
						'width' => 750,
						'height' => 100,
						'tmp_dir' => 'userfiles/_tmpfile/',
						'dir' => 'userfiles/images/rekl/',
						'filename' => $filename,
						'extension' => $extension,
					];
					Helper::uploadImage( $optionImage );
					// update nazwy pliku artykułu
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}.{$extension}' WHERE id = {$id}" );
					$sth->execute();
				} else {
					$img = explode( "." , $this->data['reklama']['image'] );
					@rename( 'userfiles/images/rekl/' . $this->data['reklama']['image'], 'userfiles/images/rekl/' . $filename . '.' . end( $img ) );
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}." . end( $img ) . "' WHERE id = {$id}" );
					$sth->execute();
				}

				if( !$_SESSION[I_ERROR] && !$_SESSION[I_INFO] )
					$_SESSION[I_SUCCESS] = "Poprawnie edytowano wpis";
			} else {
				$_SESSION[I_ERROR] = "Nie wprowadzono zmian we wpisie";
			}

			header( "Location: " . BASE . "admin_reklama/widok_top" );
			exit();
		}
	}

/**
 * [dodaj dodaj bocznę reklamę]
 * @return [type] [description]
 */
	function dodaj() {
		$this->getUser();
		$uid = $this->data['user_log']['id'];
		$id = (int)Routing::$routing['param'];

		if( $_POST['add'] == 1 ) {	
			$this->data['tytul_pl'] = strip_tags( trim( $_POST['tytul_pl'] ) );
			$this->data['link'] = strip_tags( trim( $_POST['link'] ) );
			$this->data['data_start'] = strip_tags( trim( $_POST['data_start'] ) );
			$this->data['godzina_start'] = strip_tags( trim( $_POST['godzina_start'] ) );
			$this->data['data_stop'] = strip_tags( trim( $_POST['data_stop'] ) );
			$this->data['godzina_stop'] = strip_tags( trim( $_POST['godzina_stop'] ) );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			$data_start = $this->data['data_start'] . ' ' . $this->data['godzina_start'] . ':00';
			$data_stop = $this->data['data_stop'] . ' ' . $this->data['godzina_stop'] . ':00';

			if( !$_POST['tytul_pl'] )
				throw new modelException( "Nie wprowadzono tytułu wpisu", 1 );

			// tworzę nazwę pliku na bazie tytułu artykułu
			$filename = Helper::uri_string( $this->data['tytul_pl'] );
			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} (id_usera, tytul_pl, link, data_dodania, data_start, data_stop, pozycja, stat ) VALUES( {$uid}, :tytul_pl, :link, NOW(), :datastart, :datastop, 1, '{$this->data[stat]}' )" );
			$sth->execute( 
				[
					':tytul_pl' => $this->data['tytul_pl'], 
					':link' => $this->data['link'], 
					':datastart' => $data_start, 
					':datastop' => $data_stop, 
				]
			 );

			if( $sth->rowCount() > 0 ) {
				$lastId = $this->pdo->lastInsertId();
				// update poazycji starych artykułów
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = (pozycja+1) WHERE id <> {$lastId}" );
				$sth->execute();

				$filename = $lastId . '-' . $filename;
				// upload pliku
				if( $_FILES['image_upload'] ) {
					$extension = end( explode( '.' , $_FILES['image_upload']['name'] ) );
					$optionImage = [
						'index' => 'image_upload',
						'min-width' => true,
						'height-auto' => true,
						'width' => 300,
						'height' => 600,
						'tmp_dir' => 'userfiles/_tmpfile/',
						'dir' => 'userfiles/images/rekl/',
						'filename' => $filename,
						'extension' => $extension,
					];
					Helper::uploadImage( $optionImage );
					// update nazwy pliku artykułu
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}.{$extension}' WHERE id = {$lastId}" );
					$sth->execute();
				}

				if( !$_SESSION[I_ERROR] && !$_SESSION[I_INFO] )
					$_SESSION[I_SUCCESS] = "Poprawnie dodano wpis";

			} else {
				throw new modelException( "Wystąpił problem z dodaniem wpisu", 1 );
				$_SESSION[I_ERROR] = "Wystąpił problem z dodaniem aktualnosci";
			}

			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();

		}
	}

/**
 * [dodaj dodaj górną reklamę]
 * @return [type] [description]
 */
	function dodaj_top() {
		$this->getUser();
		$uid = $this->data['user_log']['id'];
		$id = (int)Routing::$routing['param'];

		if( $_POST['add'] == 1 ) {	
			$this->data['tytul_pl'] = strip_tags( trim( $_POST['tytul_pl'] ) );
			$this->data['link'] = strip_tags( trim( $_POST['link'] ) );
			$this->data['data_start'] = strip_tags( trim( $_POST['data_start'] ) );
			$this->data['godzina_start'] = strip_tags( trim( $_POST['godzina_start'] ) );
			$this->data['data_stop'] = strip_tags( trim( $_POST['data_stop'] ) );
			$this->data['godzina_stop'] = strip_tags( trim( $_POST['godzina_stop'] ) );
			$this->data['stat'] = $_POST['stat'] = ( $_POST['stat'] ) ? 1 : 0;

			$data_start = $this->data['data_start'] . ' ' . $this->data['godzina_start'] . ':00';
			$data_stop = $this->data['data_stop'] . ' ' . $this->data['godzina_stop'] . ':00';

			if( !$_POST['tytul_pl'] )
				throw new modelException( "Nie wprowadzono tytułu wpisu", 1 );

			// tworzę nazwę pliku na bazie tytułu artykułu
			$filename = Helper::uri_string( $this->data['tytul_pl'] );
			// dodaję artykuł do bazy
			$sth = $this->pdo->prepare( "INSERT INTO {$this->options['Table']} (id_usera, tytul_pl, link, data_dodania, data_start, data_stop, pozycja, top, stat ) VALUES( {$uid}, :tytul_pl, :link, NOW(), :datastart, :datastop, 1, '1', '{$this->data[stat]}' )" );
			$sth->execute( 
				[
					':tytul_pl' => $this->data['tytul_pl'], 
					':link' => $this->data['link'], 
					':datastart' => $data_start, 
					':datastop' => $data_stop, 
				]
			 );

			if( $sth->rowCount() > 0 ) {
				$lastId = $this->pdo->lastInsertId();
				// update poazycji starych artykułów
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = (pozycja+1) WHERE id <> {$lastId}" );
				$sth->execute();

				$filename = $lastId . '-' . $filename;
				// upload pliku
				if( $_FILES['image_upload'] ) {
					$extension = end( explode( '.' , $_FILES['image_upload']['name'] ) );
					$optionImage = [
						'index' => 'image_upload',
						// 'min-width' => true,
						// 'height-auto' => true,
						'width' => 750,
						'height' => 100,
						'tmp_dir' => 'userfiles/_tmpfile/',
						'dir' => 'userfiles/images/rekl/',
						'filename' => $filename,
						'extension' => $extension,
					];
					Helper::uploadImage( $optionImage );
					// update nazwy pliku artykułu
					$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET image = '{$filename}.{$extension}' WHERE id = {$lastId}" );
					$sth->execute();
				}

				if( !$_SESSION[I_ERROR] && !$_SESSION[I_INFO] )
					$_SESSION[I_SUCCESS] = "Poprawnie dodano wpis";

			} else {
				throw new modelException( "Wystąpił problem z dodaniem wpisu", 1 );
				$_SESSION[I_ERROR] = "Wystąpił problem z dodaniem aktualnosci";
			}

			header( "Location: " . BASE . "admin_reklama/widok_top" );
			exit();

		}
	}

	function dol() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];

		$sth = $this->pdo->prepare( "SELECT pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['pozycja'] + 1;

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE pozycja = $next LIMIT 1" );
		$sth->execute();

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja + 1 ) WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono pozycję wpisu";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem ze zmianą pozycji wpisu";
		header( "Location: " . BASE . $this->options['Redirect'] );		
		exit();
	}

	function gora() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];

		$sth = $this->pdo->prepare( "SELECT pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$prev = $this->data['pos']['pozycja'] - 1;

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja + 1 ) WHERE pozycja = $prev LIMIT 1" );
		$sth->execute();

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET pozycja = ( pozycja - 1 ) WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono pozycję wpisu";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem ze zmianą pozycji wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
}
