<?php

class admin_sliderModel extends Model {

	public $options = [ 'Table' => 'artykuly', 'Redirect' => 'admin_slider',];
	public $data = [ 'admin' => true ];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$this->getUser();
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'tytul_' . LANG;
		
		$this->data['search'] = trim( strip_tags( $_GET['search'] ) );
		$search = ( $_GET['search'] ) ? " AND {$this->options['SearchCol']} REGEXP '" . trim( strip_tags( $_GET['search'] ) ) . "'" : '';

		// elementy slidera
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE slider = '1' AND stat = '1' ORDER BY slider_pozycja LIMIT " . SLIDER_ELEMENTS );
		$sth->execute();
		$this->data['slider'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $this->data['slider'] as &$aData ) {
			if( $aData['id_usera'] !== NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE id = {$aData['id_usera']} LIMIT 1" );
				$sth->execute();
				$aData['user'] = $sth->fetch( PDO::FETCH_ASSOC );
			}
		}

		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE slider = '1' AND stat = '1'" );
		$sth->execute();
		$this->data['slider_count'] = $sth->fetch( PDO::FETCH_ASSOC );

		// artykuły
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE stat = '1' AND slider = '0' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['artykuly'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		foreach ( $this->data['artykuly'] as &$aData ) {
			if( $aData['id_usera'] !== NULL ) {
				$sth = $this->pdo->prepare( "SELECT * FROM " . ADMIN_TABLE . " WHERE id = {$aData['id_usera']} LIMIT 1" );
				$sth->execute();
				$aData['user'] = $sth->fetch( PDO::FETCH_ASSOC );
			}
		}

		$this->data['limit_slides'] = ( count( $this->data['slider'] ) < SLIDER_ELEMENTS ) ? true : false;


		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

	}

	function wlacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '1' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie włączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z włączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function wylacz() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$this->data['admin'] = true;
		$id = (int)Routing::$routing['param'];
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET stat = '0' WHERE id = $id" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie wyłączono wpis";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem z wyłączeniem wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}
	function usun() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];
		$c = Routing::$routing['title'];

		if( $c != 'confirm' ) {
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT image, pozycja, slider_pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( "Element slidera nie istnieje", 1 );

		$this->data['spos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['spos']['slider_pozycja'];

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider = '0', slider_pozycja = NULL WHERE id = $id" );
		if( !$sth->execute() )
			throw new modelException( "Wystąpił błąd podczas usuwania elementu ze slidera", 1 );

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = ( slider_pozycja - 1 ) WHERE slider_pozycja > $next" );
		$sth->execute();

		$_SESSION[I_SUCCESS] = "Poprawnie usunięto element slidera";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function edytuj() {
		$this->getUser();
		$id = (int)Routing::$routing['param'];

		if( !$id )
			throw new modelException( "Brak wymaganego parametru", 1 );
		
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() < 1 )
			throw new modelException( "Szukany wpis nie istnieje", 1);
			
		$this->data['kategoria'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( $_POST['edit'] == 1 ) {
			$this->data['nazwa_pl'] = strip_tags( trim( $_POST['nazwa_pl'] ) );
			// dodaję kategorię do bazy
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET nazwa_pl = :nazwa_pl WHERE id = {$id}" );
			var_dump( "UPDATE {$this->options['Table']} SET nazwa_pl = :nazwa_pl WHERE id = {$id}" );
			if( $sth->execute( [':nazwa_pl' => $this->data['nazwa_pl']] ) ) {
				if( $sth->rowCount() > 0 )
					$_SESSION[I_SUCCESS] = "Poprawnie edytowano wpis";
				else
					$_SESSION[I_SUCCESS] = "Poprawnie zapisano wpis, jednak nic w nim nie zostało zmienione";
			} else {
				$_SESSION[I_ERROR] = "Nie wprowadzono zmian we wpisie";
			}
			header( "Location: " . BASE . $this->options['Redirect'] );
			exit();
		}
	}

	function dodaj() {
		$this->getUser();
		$uid = $this->data['user_log']['id'];
		$id = (int)Routing::$routing['param'];

		// elementy slidera
		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE slider = '1' AND stat = '1' ORDER BY slider_pozycja LIMIT " . SLIDER_ELEMENTS );
		$sth->execute();
		$this->data['slider'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		if( count( $this->data['slider'] ) >= SLIDER_ELEMENTS )
			throw new modelException( "Nie możesz dodać elementu do slidera. Slider może posiadać maksymalnie " . SLIDER_ELEMENTS . " elementów.", 1);

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider = '1', slider_pozycja = 1 WHERE id = {$id}" );
		if( !$sth->execute() )
			throw new modelException( "Wystąpił problem z dodaniem elementu do slidera", 1);
		
		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = (slider_pozycja+1) WHERE id <> {$id}" );
		$sth->execute();

		$_SESSION[I_SUCCESS] = "Poprawnie dodano element do slidera";
		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function dol() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];

		$sth = $this->pdo->prepare( "SELECT slider_pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$next = $this->data['pos']['slider_pozycja'] + 1;

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = ( slider_pozycja - 1 ) WHERE slider_pozycja = $next LIMIT 1" );
		$sth->execute();

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = ( slider_pozycja + 1 ) WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono pozycję wpisu";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem ze zmianą pozycji wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

	function gora() {
		if( !Auth::sessionAuthExist() || Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) {
			header( "Location: " . BASE . "index/admin" );
			exit;
		}
		$id = (int)Routing::$routing['param'];

		$sth = $this->pdo->prepare( "SELECT slider_pozycja FROM {$this->options['Table']} WHERE id = $id LIMIT 1" );
		$sth->execute();
		$this->data['pos'] = $sth->fetch( PDO::FETCH_ASSOC );
		$prev = $this->data['pos']['slider_pozycja'] - 1;

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = ( slider_pozycja + 1 ) WHERE slider_pozycja = $prev LIMIT 1" );
		$sth->execute();

		$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET slider_pozycja = ( slider_pozycja - 1 ) WHERE id = $id LIMIT 1" );
		$sth->execute();

		if( $sth->rowCount() > 0 )
			$_SESSION[I_SUCCESS] = "Poprawnie zmieniono pozycję wpisu";
		else
			$_SESSION[I_ERROR] = "Wystąpił problem ze zmianą pozycji wpisu";

		header( "Location: " . BASE . $this->options['Redirect'] );
		exit();
	}

}
