<?php

class firmyModel extends Model {

	public $options = [ 'Table' => 'firmy', 'Redirect' => '', 'SearchCol' => '',];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		$id = (int)Routing::$routing['param'];

		if( !$id ) {
			header("Location: " . BASE . "firmy/wszystkie");
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT(data_dodania, '%d.%m.%Y') as data FROM {$this->options['Table']} WHERE id = {$id} AND stat = '1' LIMIT 1" );
		if( !$sth->execute() )
			throw new modelException( $this->data['dictionary'][97][LANG], 1 );

		$this->data['firmy'] = $sth->fetch( PDO::FETCH_ASSOC );
	}

	function wszystkie() {
		$client = (int)$_SESSION[AUTH_SESSION_NAME]['client'];
		$uid = (int)$_SESSION[AUTH_SESSION_NAME]['im'];

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;
		$this->options['SearchCol'] = 'nazwa_' . LANG;

		$this->data['search'] = trim( strip_tags( $_GET['szukaj'] ) );
		$search = ( $_GET['szukaj'] ) ? " AND LOWER( nazwa_pl ) REGEXP '" . mb_strtolower( trim( strip_tags( $_GET['szukaj'] ) ), 'UTF-8' ) . "' OR LOWER( miasto ) REGEXP '" . mb_strtolower( trim( strip_tags( $_GET['szukaj'] ) ), 'UTF-8' ) . "'" : '';
		// var_dump( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' {$search} ORDER BY pozycja LIMIT {$limit} OFFSET {$offset}" );
		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data FROM {$this->options['Table']} WHERE stat = '1' {$search} ORDER BY nazwa_pl LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['firmy'] = $sth->fetchAll( PDO::FETCH_ASSOC );


		$sth = $this->pdo->prepare( "SELECT COUNT(*) as TOTAL FROM {$this->options['Table']} WHERE stat = '1' {$search}" );
		$sth->execute();
		$total = $sth->fetch( PDO::FETCH_ASSOC );
		$this->data['TOTAL'] = $total['TOTAL'];

		$this->data['seo'] = [
			'title' => ' - ' . $this->data['dictionary'][57][LANG],
			'description' => $this->data['dictionary'][50][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . $this->data['dictionary'][57][LANG],
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];
	}

}
