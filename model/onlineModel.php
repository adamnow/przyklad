<?php

class onlineModel extends Model {

	public $options = [ 'Table' => 'czasopisma', 'Redirect' => '', 'SearchCol' => 'tytul_' . LANG,];

	function index() {
		// var_dump( "Model" );
	}

	function widok() {
		if( !$_COOKIE[COOKIES_ARTICLE_ACCESS] ) {
			header( "Location: " . BASE );
			exit();
		}
		$id = (int)Routing::$routing['param'];
		$token = trim( strip_tags( Routing::$routing['title'] ) );

		if( !$id || !$token ) {
			$_SESSION[I_ERROR] = $this->data['dictionary'][108][LANG];
			header("Location: " . BASE . "online/wszystkie" );
			exit();
		}

		$sth = $this->pdo->prepare( "SELECT * FROM {$this->options['Table']} WHERE id = {$id} AND token = '{$token}' LIMIT 1" );
		$sth->execute();
		$this->data['target'] = $sth->fetch( PDO::FETCH_ASSOC );

		if( !$this->data['target'] ) {
			$_SESSION[I_ERROR] = $this->data['dictionary'][109][LANG];
			header("Location: " . BASE . "online/wszystkie" );
			exit();
		}
			
		// dodaję liczbę wyswietleń artykułu i ustawiam ciasteczko
		$cookie = ( $_COOKIE[COOKIES_ONLINE] ) ? unserialize( $_COOKIE[COOKIES_ONLINE] ) : [];
		if( $cookie ) {
			if( !in_array( $id , $cookie ) ) {
				$show = $this->data['target']['wyswietlenia'] + 1;
				$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
				$sth->execute();
				array_push( $cookie , $id );
				$new_cookie = serialize( $cookie );
				setcookie( COOKIES_ONLINE, $new_cookie, time() + (60 * COOKIES_ONLINE_TIMEOUT), "/");
			}
		} else {
			$show = (int)$this->data['target']['wyswietlenia'] + 1;
			$sth = $this->pdo->prepare( "UPDATE {$this->options['Table']} SET wyswietlenia = {$show} WHERE id = {$id}" );
			$sth->execute();
			array_push( $cookie , $id );
			$new_cookie = serialize( $cookie );
			setcookie( COOKIES_ONLINE, $new_cookie, time() + (60 * COOKIES_ONLINE_TIMEOUT), "/");
		}

		$this->data['seo'] = [
			'title' => ' - ' . html_entity_decode( $this->data['target']['tytul_' . LANG] ),
			'description' => $this->data['dictionary'][50][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . ' Czytaj on-line',
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => ( @file_get_contents( BASE . 'userfiles/images/online/' . $this->data['target']['image'] ) ) ? BASE . 'userfiles/images/online/' . $this->data['target']['image'] : BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

		

		// header( "Location: " . $this->data['target']['issue_link']  );
		// exit();

	}

	function wszystkie() {
		if( !$_COOKIE[COOKIES_ARTICLE_ACCESS] ) {
			header( "Location: " . BASE );
			exit();
		}

		$limit = PERPAGE;
		$offset = ( !$_GET['p'] ) ? 0 : ( (int)$_GET['p'] - 1 ) * PERPAGE;

		$sth = $this->pdo->prepare( "SELECT *, DATE_FORMAT( data_dodania, '%d.%m.%Y' ) as data, DATE_FORMAT( data_publikacji, '%d.%m.%Y' ) as data_publikacji_format FROM {$this->options['Table']} WHERE stat = '1' ORDER BY data_publikacji DESC LIMIT {$limit} OFFSET {$offset}" );
		$sth->execute();
		$this->data['online'] = $sth->fetchAll( PDO::FETCH_ASSOC );

		$this->data['seo'] = [
			'title' => ' - Czytaj on-line',
			'description' => $this->data['dictionary'][50][LANG],
			'keywords' => $this->data['dictionary'][48][LANG] . ' Czytaj on-line',
			'author' => $this->data['dictionary'][49][LANG],
			'url' => ( isset( $_SERVER['HTTPS'] ) ? "https" : "http" ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
			'type' => 'article',
			'image' => BASE . 'userfiles/images/logo/secandas-logo.jpg',
		];

	}



}
