<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
    <div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    
    <div class="col-sm-8 left-s-ctn article-show">
        <h1><?= $this->data['dictionary'][53][LANG]; ?></h1>
        <span class="article-date">#<?= $this->data['tag']; ?></span>
        <!-- wydarzenia -->
        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <?php if( $this->data['wydarzenia'] ) : ?>
            <div class="row events">
                <?php foreach( $this->data['wydarzenia'] as $aData ) : ?>
                    <div class="event-i">
                        <figure class="col-sm-4">
                            <?php if( @file_get_contents( 'userfiles/images/wydarzenia/' . $aData['image'] ) ) : ?>
                                <img src="<?= BASE; ?>userfiles/images/wydarzenia/<?= $aData['image']; ?>" alt="">
                            <?php else : ?>
                                <div class="no-image"></div>
                            <?php endif; ?>
                        </figure>
                        <div class="col-sm-8 e-txt">
                            <p class="e-date"><?= $aData['data']; ?></p>
                            <p class="e-title"><?= $aData['tytul_' . LANG]; ?></p>
                            <div class="e-ctn"><?= $aData['zajawka_' . LANG]; ?></div>
                        </div>
                        <div class="link-group"><a href="<?= BASE; ?>wydarzenia/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_' . LANG] ); ?>" class='more'>Przejdź do strony</a></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- ! wydarzenia -->
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>

        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <div class="row">
            <?= $page; ?>
        </div>

    </div>
    <!-- ! lewa strona -->


    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>