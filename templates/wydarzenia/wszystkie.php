<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
    <div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    
    <div class="col-sm-8 left-s-ctn article-show">
        <h1><?= $this->data['dictionary'][53][LANG]; ?></h1>
        <span class="article-date">&nbsp;</span>
        <!-- kalendarium -->
        <div class="row calendar">
            <div class="col-sm-12">
                <!-- filtrowanie -->
                <div class="filters filters-page">
                    <form>
                        <?php if( $this->data['eventCategories'] ) : ?>
                            <div class="select-form select-form-long">
                                <select name="f[cat]" id="category">
                                    <option value=""><?= $this->data['dictionary'][93][LANG]; ?></option>
                                    <?php foreach( $this->data['eventCategories'] as $aData ) : ?>
                                        <?php $select = ( $_GET['f']['cat'] == $aData['id'] ) ? 'selected' : ''; ?>
                                        <option value="<?= $aData['id']; ?>" <?= $select; ?>><?= $aData['nazwa_' . LANG]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="dropdown"><i class="icon-angle-down"></i></span>
                            </div>
                        <?php endif; ?>
                        <div class="select-form">
                            <select name="f[m]" id="month">
                                <option value=""><?= $this->data['dictionary'][13]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 1 ) echo "selected"; ?> value="1"><?= $this->data['dictionary'][33]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 2 ) echo "selected"; ?> value="2"><?= $this->data['dictionary'][34]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 3 ) echo "selected"; ?> value="3"><?= $this->data['dictionary'][35]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 4 ) echo "selected"; ?> value="4"><?= $this->data['dictionary'][36]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 5 ) echo "selected"; ?> value="5"><?= $this->data['dictionary'][37]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 6 ) echo "selected"; ?> value="6"><?= $this->data['dictionary'][38]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 7 ) echo "selected"; ?> value="7"><?= $this->data['dictionary'][39]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 8 ) echo "selected"; ?> value="8"><?= $this->data['dictionary'][40]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 9 ) echo "selected"; ?> value="9"><?= $this->data['dictionary'][41]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 10 ) echo "selected"; ?> value="10"><?= $this->data['dictionary'][42]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 1 ) echo "selected"; ?> value="11"><?= $this->data['dictionary'][43]['pl']; ?></option>
                                <option <?php if( $_GET['f']['m'] == 1 ) echo "selected"; ?> value="12"><?= $this->data['dictionary'][44]['pl']; ?></option>
                            </select>
                            <span class="dropdown"><i class="icon-angle-down"></i></span>
                        </div>
                        <div class="select-form select-form-short">
                            <select name="f[y]" id="year">
                                <option value=""><?= $this->data['dictionary'][14]['pl']; ?></option>
                                <?php for( $y = $this->data['rok']['rok']; $y <= date('Y'); $y++ ) : ?>
                                    <?php $select = ( $_GET['f']['y'] == $y ) ? 'selected' : ''; ?>
                                    <option value="<?= $y; ?>" <?= $select; ?>><?= $y; ?></option>
                                <?php endfor; ?>
                            </select>
                            <span class="dropdown"><i class="icon-angle-down"></i></span>
                        </div>
                        <button class='red-btn'><?= $this->data['dictionary'][94][LANG]; ?></button>
                    </form>
                </div>
                <?php if( $_GET['f'] && $this->data['wydarzenia'] ) : ?>
                    <div class="results-count"><?= $this->data['dictionary'][95][LANG]; ?> <span><?= $this->data['TOTAL']; ?></span></div>
                <?php endif; ?>
                <!-- ! filtrowanie -->
            </div>
        </div>
        <!-- ! kalendarium -->
        <!-- wydarzenia -->
        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <?php if( $this->data['wydarzenia'] ) : ?>
            <div class="row events">
                <?php foreach( $this->data['wydarzenia'] as $aData ) : ?>
                    <div class="event-i">
                        <figure class="col-sm-4">
                            <?php if( @file_get_contents( 'userfiles/images/wydarzenia/' . $aData['image'] ) ) : ?>
                                <img src="<?= BASE; ?>userfiles/images/wydarzenia/<?= $aData['image']; ?>" alt="">
                            <?php else : ?>
                                <div class="no-image"></div>
                            <?php endif; ?>
                        </figure>
                        <div class="col-sm-8 e-txt">
                            <p class="e-date"><?= $aData['data']; ?></p>
                            <p class="e-title"><?= $aData['tytul_' . LANG]; ?></p>
                            <div class="e-ctn"><?= $aData['zajawka_' . LANG]; ?></div>
                        </div>
                        <div class="link-group"><a href="<?= BASE; ?>wydarzenia/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][90][LANG]; ?></a></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <!-- ! wydarzenia -->
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>

        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <div class="row">
            <?= $page; ?>
        </div>

    </div>
    <!-- ! lewa strona -->


    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>