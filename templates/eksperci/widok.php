<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show">
        <?php if( $this->data['eksperci'] ) : ?>
            <h1><?= $this->data['eksperci']['imie']; ?> <?= $this->data['eksperci']['nazwisko']; ?></h1>
            <span class="article-date">&nbsp;</span>
            <div class="article-ctn">
                
                    <div class="col-sm-5">
                        <figure class='expert-img'>
                            <?php if( @file_get_contents( 'userfiles/images/eksperci/' . $this->data['eksperci']['image'] ) ) : ?>
                                <figure><img src="<?= BASE; ?>userfiles/images/eksperci/<?= $this->data['eksperci']['image']; ?>" alt=""></figure>
                            <?php else : ?>
                                <div class="no-image"></div>
                            <?php endif; ?>
                        </figure>
                    </div>
                    <div class="col-sm-7 expert-detail">
                        <h3 class="expert-name"><?= $this->data['eksperci']['imie']; ?> <?= $this->data['eksperci']['nazwisko']; ?></h3>
                        <?php if( $this->data['eksperci']['stanowisko'] ) : ?>
                            <h3 class="expert-position"><?= $this->data['dictionary'][78][LANG]; ?>: <strong><?= $this->data['eksperci']['stanowisko']; ?></strong></h3>
                        <?php endif; ?>
                        <?php if( $this->data['eksperci']['firma'] ) : ?>
                            <h3 class="expert-corp"><?= $this->data['dictionary'][77][LANG]; ?>: <strong><?= $this->data['eksperci']['firma']; ?></strong></h3>
                        <?php endif; ?>
                        <?php if( $this->data['eksperci']['bio'] ) : ?>
                            <div class="expert-short">
                                <?= $this->data['eksperci']['bio']; ?>
                            </div>
                        <?php endif; ?>
                    </div>
            </div>
            <?php if( $this->data['eksperci']['tresc_' . LANG] ) : ?>
                <div class="article-ctn expert-long-ctn">
                    <div class="expert-long">
                        <strong><?= $this->data['dictionary'][85][LANG]; ?></strong><br><br>
                        <?= $this->data['eksperci']['tresc_' . LANG]; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>

        <div class="link-group text-left" style='text-align: left;'>
            <a href='<?= BASE; ?>eksperci/wszystkie' class="more text-left"><?= $this->data['dictionary'][84][LANG]; ?></a>
        </div>
        
    </div>
    <!-- ! lewa strona -->
    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>