
<?php if( $this->data['main_partnerzy'] ) : ?>
    <div class="row events companies">
        <h3 class='title-lines'>
            <span><?= $this->data['dictionary'][138]['pl']; ?></span>
        </h3>
        <div class="partners row">
            <?php foreach( $this->data['main_partnerzy'] as $aData ) : ?>
                <figure class='col-sm-3 partners-image'>
                    <?php if( @file_get_contents( "userfiles/images/partnerzy/{$aData['image']}" ) ) : ?>
                        <img src="<?= BASE; ?>userfiles/images/partnerzy/<?= $aData['image']; ?>" alt="">
                    <?php else : ?>
                        <div class="no-image"></div>
                    <?php endif; ?>
                    <a href="<?= $aData['www']; ?>" target='_blank'></a>
                </figure>
            <?php endforeach; ?>
        </div>
        
    </div>
<?php endif; ?>



