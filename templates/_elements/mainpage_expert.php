<?php /*
<?php if( $this->data['experts'] ) : ?>
	<div class="section-i expert">
	    <h3><?= $this->data['dictionary'][21]['pl']; ?></h3>
	    <figure>
	    	<?php if( @file_get_contents( 'userfiles/images/eksperci/' . $this->data['experts'][0]['image'] ) ) : ?>
	    		<img src="<?= BASE; ?>userfiles/images/eksperci/<?= $this->data['experts'][0]['image']; ?>" alt="">
	    	<?php else : ?>
	    		<div class="no-image"></div>
	    	<?php endif; ?>
	    </figure>
	    <div class="expert-txt">
	        <h4><?= $this->data['experts'][0]['imie']; ?> <?= $this->data['experts'][0]['nazwisko']; ?></h4>
	        <p><?= strip_tags( $this->data['experts'][0]['bio'] ); ?></p>
	    </div>
	    <div class="link-group"><a href="<?= BASE; ?>eksperci/widok/<?= $this->data['experts'][0]['id']; ?>/<?= Helper::uri_string( $this->data['experts'][0]['imie'] . ' ' . $this->data['experts'][0]['nazwisko'] ); ?>" class='more'>Zobacz</a></div>
	</div>
<?php endif; ?>
*/ ?>