
<?php if( $this->data['newsroom'] ) : ?>
    <div class="section-i">
        <h3><?= $this->data['dictionary'][23]['pl']; ?></h3>
        <?php foreach( $this->data['newsroom'] as $aData ) : ?>
            <div class="row section-i-item">
                <div class="col-md-6">
                    <figure>
                        <?php if( @file_get_contents( "userfiles/images/newsroom/" . $aData['image'] ) ) : ?>
                            <img src="<?= BASE; ?>userfiles/images/newsroom/<?= $aData['image']; ?>" alt="">
                        <?php else : ?>
                            <div class="no-image"></div>
                        <?php endif; ?>
                    </figure>
                </div>
                <div class="col-md-6 section-i-txt"><?= $aData['tytul_' . LANG] ?></div>
                <span class="date"><?= $aData['data'] ?></span>
                <a href="<?= BASE; ?>newsroom/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_pl'] ); ?>"></a>
            </div>
            <div class="separator-line"></div>
        <?php endforeach; ?>
        <div class="link-group"><a href="<?= BASE; ?>newsroom/wszystkie" class='more'><?= $this->data['dictionary'][26]['pl']; ?></a></div>
    </div>
<?php endif; ?>
