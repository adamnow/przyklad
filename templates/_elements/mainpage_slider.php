<?php if( $this->data['slider'] ) : ?>
    <div id="slider">
        <!-- 750x490px -->
        <?php foreach( $this->data['slider'] as $aData ) : ?>
            <div class="slider-i">
                <div class="slide-title">
                    <h2><?= $aData['tytul_' . LANG]; ?></h2>
                    <span><?= $aData['data']; ?></span>
                </div>
                <img src="<?= BASE; ?>userfiles/images/artykuly/<?= $aData['image']; ?>" alt="">
                <?php $controller = ( $aData['news'] == 1 ) ? 'news' : 'artykul'; ?>
                <a href="<?= BASE; ?><?= $controller; ?>/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>"></a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>