<form id='form-search'>
	<?php if( $this->data['search'] ) : ?>
		<button id="clear-input"><i class="icon-cancel-circled-outline"></i></button>
	<?php endif; ?>
	<input type="search" name='search' placeholder='Szukaj' value='<?= $this->data['search']; ?>'>
	<button>Szukaj</button>
</form>