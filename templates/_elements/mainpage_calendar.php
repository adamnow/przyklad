<?php if( $this->data['wydarzenia'] ) : ?>
    <!-- kalendarium -->
    <div class="row calendar">
        <div class="col-sm-12">
            <h3 class='title-lines'>
                <span><?= $this->data['dictionary'][12]['pl']; ?></span>
            </h3>
            <!-- filtrowanie -->
            <div class="filters">
                <form action="<?= BASE; ?>wydarzenia/wszystkie">
                    <div class="select-form">
                        <select name="f[m]" id="month">
                            <option value=""><?= $this->data['dictionary'][13]['pl']; ?></option>
                            <option value="1"><?= $this->data['dictionary'][33]['pl']; ?></option>
                            <option value="2"><?= $this->data['dictionary'][34]['pl']; ?></option>
                            <option value="3"><?= $this->data['dictionary'][35]['pl']; ?></option>
                            <option value="4"><?= $this->data['dictionary'][36]['pl']; ?></option>
                            <option value="5"><?= $this->data['dictionary'][37]['pl']; ?></option>
                            <option value="6"><?= $this->data['dictionary'][38]['pl']; ?></option>
                            <option value="7"><?= $this->data['dictionary'][39]['pl']; ?></option>
                            <option value="8"><?= $this->data['dictionary'][40]['pl']; ?></option>
                            <option value="9"><?= $this->data['dictionary'][41]['pl']; ?></option>
                            <option value="10"><?= $this->data['dictionary'][42]['pl']; ?></option>
                            <option value="11"><?= $this->data['dictionary'][43]['pl']; ?></option>
                            <option value="12"><?= $this->data['dictionary'][44]['pl']; ?></option>
                        </select>
                        <span class="dropdown"><i class="icon-angle-down"></i></span>
                    </div>
                    <div class="select-form select-form-short">
                        <select name="f[y]" id="year">
                            <option value=""><?= $this->data['dictionary'][14]['pl']; ?></option>
                            <?php for( $y = $this->data['rok']['rok']; $y <= date('Y'); $y++ ) : ?>
                                <option value="<?= $y; ?>"><?= $y; ?></option>
                            <?php endfor; ?>
                        </select>
                        <span class="dropdown"><i class="icon-angle-down"></i></span>
                    </div>
                    <button class='red-btn'><?= $this->data['dictionary'][15]['pl']; ?></button>
                </form>
            </div>
            <!-- ! filtrowanie -->
        </div>
    </div>
    <!-- ! kalendarium -->
    <!-- wydarzenia -->
    <div class="row events">
        <?php foreach( $this->data['wydarzenia'] as $aData ) : ?>
            <div class="event-i">
                <figure class="col-sm-4">
                    <?php if( @file_get_contents( 'userfiles/images/wydarzenia/' . $aData['image'] ) ) : ?>
                        <img src="<?= BASE; ?>userfiles/images/wydarzenia/<?= $aData['image']; ?>" alt="">
                    <?php else : ?>
                        <div class="no-image"></div>
                    <?php endif; ?>
                </figure>
                <div class="col-sm-8 e-txt">
                    <p class="e-date"><?= $aData['data']; ?></p>
                    <p class="e-title"><?= $aData['tytul_' . LANG] ?></p>
                    <div class="e-ctn">
                        <?= $aData['zajawka_' .LANG]; ?>
                    </div>
                </div>
                <div class="link-group"><a href="<?= BASE; ?>wydarzenia/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][16]['pl']; ?></a></div>
            </div>
        <?php endforeach;?>
        <?php if( count( $this->data['wydarzenia'] ) > 3 ) : ?>
            <div class="link-group"><a href="<?= BASE; ?>wydarzenia" class='more'><?= $this->data['dictionary'][17]['pl']; ?></a></div>
        <?php endif; ?>

    </div>
    <!-- ! wydarzenia -->
<?php endif; ?>

