<?php if( $this->data['mostPopular'] ) : ?>
    <div class="section-i">
        <h3><?= $this->data['dictionary'][27]['pl']; ?></h3>
        <?php foreach( $this->data['mostPopular'] as $aData ) : ?>
            <div class="row section-i-item">
                <div class="col-md-5">
                    <?php if( @file_get_contents( 'userfiles/images/artykuly/' . $aData['image'] ) ) : ?>
                        <figure>
                            <img src="<?= BASE; ?>userfiles/images/artykuly/<?= $aData['image']; ?>" alt="">
                        </figure>
                    <?php endif; ?>
                </div>
                <div class="col-md-7 section-i-txt">
                    <?= $aData['tytul_' . LANG] ?>
                </div>
                <span class="date"><?= $aData['data'] ?></span>
                <?php $controller = ( $aData['news'] == 1 ) ? 'news' : 'artykul'; ?>
                <a href="<?= BASE; ?><?= $controller; ?>/widok/<?= $aData['id'] ?>/<?= explode( '.' , $aData['image'] )[0]; ?>"></a>
            </div>
            <div class="separator-line"></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>