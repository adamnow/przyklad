<?php if( $this->data['last_magazine'] ) : ?>
	<div class="section-i secandas">
	    <h3><?= $this->data['dictionary'][28]['pl']; ?></h3>
	    <figure>
	    	<?php if( @file_get_contents( 'userfiles/images/online/' . $this->data['last_magazine']['image'] ) ) : ?>
	        	<a href="<?= BASE; ?>czasopismo"><img src="<?= BASE; ?>userfiles/images/online/<?= $this->data['last_magazine']['image']; ?>" alt=""></a>
	    	<?php else : ?>
	    		<div class="no-image"></div>
	    	<?php endif; ?>
	    </figure>
	    <div class="link-group link-group-center"><a href="<?= BASE; ?>czasopismo" class='more'><?= $this->data['dictionary'][25]['pl']; ?></a></div>
	</div>
<?php endif; ?>

<div class="fb-page" data-href="https://www.facebook.com/secandas/" data-tabs="timeline" data-width="350" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" style='margin: 20px auto 30px;'>
	<blockquote cite="https://www.facebook.com/secandas/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/secandas/">SEC&amp;AS</a>
	</blockquote>
</div>