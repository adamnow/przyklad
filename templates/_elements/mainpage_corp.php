
<?php if( $this->data['main_firmy'] ) : ?>
    <div class="row events companies">
        <h3 class='title-lines'>
            <span><?= $this->data['dictionary'][57]['pl']; ?></span>
        </h3>
        <?php foreach( $this->data['main_firmy'] as $aData ) : ?>
            <div class="event-i corp-i">
                <figure class="col-sm-4">
                    <?php if( @file_get_contents( "userfiles/images/firmy/{$aData['image']}" ) ) : ?>
                        <img src="<?= BASE; ?>userfiles/images/firmy/<?= $aData['image']; ?>" alt="">
                    <?php else : ?>
                        <div class="no-image"></div>
                    <?php endif; ?>
                </figure>
                <div class="col-sm-8 e-txt">
                    <p class="e-title"><?= $aData['nazwa_' . LANG]; ?></p>

                    <div class="corp-detail-div">
                        <?php if( $aData['adres'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-home"></i>
                                <?= $aData['adres']; ?><br><?= $aData['kod']; ?> <?= $aData['miasto']; ?>
                            </span>
                        <?php endif; ?>
                        <br>
                        <?php if( $aData['telefon'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-phone"></i>
                                <a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $aData['telefon'] ); ?>" target='_blank'><?= $aData['telefon']; ?></a>
                            </span>
                        <?php endif; ?>
                        <?php if( $aData['email'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-mail"></i>
                                <a href="mailto:<?= $aData['email']; ?>" target='_blank'><?= $aData['email']; ?></a>
                            </span>
                        <?php endif; ?>
                        <?php if( $aData['fax'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-print"></i>
                                <?= $aData['fax']; ?>
                            </span>
                        <?php endif; ?>
                        <?php if( $aData['www'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-monitor"></i>
                                <a href='<?php if( preg_match( "/^(http:\/\/)/", $aData['www'] ) ) { echo $aData['www']; } else { echo "http://" . $aData['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $aData['www'] ); ?></a>
                            </span>
                        <?php endif; ?>
                    </div>

                    <?php /*
                    <table class="corp-datail">
                        <tbody>
                            <tr>
                                <td class='td-ico'><i class="icon-home"></i></td>
                                <td><?= $aData['adres']; ?><br><?= $aData['kod']; ?> <?= $aData['miasto']; ?></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class='td-ico'><i class="icon-phone"></i></td>
                                <td><a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $aData['telefon'] ); ?>" target='_blank'><?= $aData['telefon']; ?></a></td>
                                <td class='td-ico'><i class="icon-mail"></i></td>
                                <td>
                                    <a href="mailto:<?= $aData['email']; ?>" target='_blank'><?= $aData['email']; ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class='td-ico'><i class="icon-print"></i></td>
                                <td><?= $aData['fax']; ?></td>
                                <td class='td-ico'><i class="icon-monitor"></i></td>
                                <td>
                                    <a href='<?php if( preg_match( "/^(http:\/\/)/", $aData['www'] ) ) { echo $aData['www']; } else { echo "http://" . $aData['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $aData['www'] ); ?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    */ ?>
                </div>
                <?php if( !$aData['opis_' . LANG] ) : ?>
                    <div class="link-group"><a href="<?= BASE; ?>firmy/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][19]['pl']; ?></a></div>
                <?php endif; ?>
            </div>
            <?php if( $aData['opis_' . LANG] ) : ?>
                <div class="event-i-det-c">
                    <?= $aData['opis_' . LANG]; ?>
                    <div class="link-group"><a href="<?= BASE; ?>firmy/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][19]['pl']; ?></a></div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        
    </div>
<?php endif; ?>

<div class="link-group"><a href="firmy/wszystkie" class='more'><?= $this->data['dictionary'][20]['pl']; ?></a></div>


