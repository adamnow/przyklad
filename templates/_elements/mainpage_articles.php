<?php if( $this->data['artykuly'] ) : ?>
    <div class="row articles">
        <div class="col-sm-12">
            <h3 class='title-lines'>
                <span><?= $this->data['dictionary'][11]['pl']; ?></span>
            </h3>

            <div class="articles-box row">
                <?php $ind = 1; ?>
                <?php foreach( $this->data['artykuly'] as $aData ) : ?>
                    <?php if( $ind <= 4 ) : ?>
                        <?php
                            $dir = ( $aData['strona_glowna'] == 1 ) ? 'newsroom' : 'artykuly';
                            $action = ( $aData['strona_glowna'] == 1 ) ? 'newsroom' : 'artykul';

                            if( $action == 'artykul' )
                                $action = ( $aData['news'] == 1 ) ? 'news' : 'artykul';
                        ?>
                        <!-- artykuły kwadraty -->
                        <div class="col-sm-6 article-item article-item-box article-overflow">
                            <div class="article-i">
                                <div class="article-title">
                                    <h2><?= $aData['tytul_' . LANG]; ?></h2>
                                    <span><?= $aData['data']; ?></span>
                                </div>
                                <?php  ?>
                                <?php if( @file_get_contents( 'userfiles/images/' . $dir . '/' . $aData['image'] ) ) : ?>
                                    <?php 
                                        $classImg = Helper::imageOrientation( 'userfiles/images/' . $dir . '/' . $aData['image'], 'class="image_vertical"' );
                                    ?>
                                    <img src="<?= BASE; ?>userfiles/images/<?= $dir; ?>/<?= $aData['image']; ?>" <?= $classImg; ?> alt="">
                                <?php else : ?>
                                    <div class="no-image"></div>
                                <?php endif; ?>
                                <?php /* <h4 class="category-title">SEC&Tech</h4> */ ?>
                                <a href="<?= BASE; ?><?= $action; ?>/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>"></a>
                                <?php if( $aData['strona_glowna'] == 1 ) : ?>
                                    <span class="stick"><?= $this->data['dictionary'][23][LANG]; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php else : ?>
                        <?php
                            $dir = ( $aData['strona_glowna'] == 1 ) ? 'newsroom' : 'artykuly';
                            $action = ( $aData['strona_glowna'] == 1 ) ? 'newsroom' : 'artykul';

                            if( $action == 'artykul' )
                                $action = ( $aData['news'] == 1 ) ? 'news' : 'artykul';
                        ?>
                        <!-- artykuły horyzontalne -->
                        <div class="col-sm-12 article-item article-item-horizontal">
                            <div class="article-i article-i-scale row">
                                <div class="col-sm-6">
                                    <?php if( @file_get_contents( 'userfiles/images/' . $dir . '/' . $aData['image'] ) ) : ?>
                                        <img src="<?= BASE; ?>userfiles/images/<?= $dir; ?>/<?= $aData['image']; ?>" alt="">
                                    <?php else : ?>
                                        <div class="no-image"></div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-sm-6 article-txt article-txt-padding">
                                    <div class="article-title">
                                        <h2><?= $aData['tytul_' . LANG]; ?></h2>
                                        <p><?= strip_tags( mb_substr( $aData['zajawka_' . LANG], 0, 300, 'utf-8') ); ?></p>
                                        <span><?= $aData['data']; ?></span>
                                    </div>
                                </div>
                                <div class="link-group">
                                    <span class="more"><?= $this->data['dictionary'][64][LANG]; ?></span>
                                </div>
                                <a href="<?= BASE; ?><?= $action; ?>/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>"></a>
                            </div>
                            <?php /*<h4 class="category-title">SEC&Tech</h4>*/ ?>
                        </div>
                    <?php endif; ?>
                    <?php $ind++; ?>
                <?php endforeach; ?>
            </div>
            <div class='link-group'><a href="<?= BASE; ?>artykuly" class="more"><?= $this->data['dictionary'][65][LANG]; ?></a></div>
        </div>
    </div>
<?php endif; ?>
