<?php if( $this->data['sub_slider'] ) : ?>
    <div class="row article-distinction">
        <!-- 750x490px -->
        <?php foreach( $this->data['sub_slider'] as $aData ) : ?>
            <div class="col-sm-6">
                <div class="article-i">
                    <div class="article-title">
                        <h2><?= $aData['tytul_' . LANG]; ?></h2>
                        <span><?= $aData['data']; ?></span>
                    </div>
                    <?php if( @file_get_contents( 'userfiles/images/artykuly/' . $aData['image'] ) ) : ?>
                        <?php 
                            $classImg = Helper::imageOrientation( 'userfiles/images/artykuly/' . $aData['image'], 'class="image_vertical"' );
                        ?>
                        <img src="<?= BASE; ?>userfiles/images/artykuly/<?= $aData['image']; ?>" alt="" <?= $classImg; ?>>
                    <?php else : ?>
                        <div class="no-image"></div>
                    <?php endif; ?>
                    <?php $controller = ( $aData['news'] == 1 ) ? 'news' : 'artykul'; ?>
                    <a href="<?= BASE; ?><?= $controller; ?>/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>"></a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>