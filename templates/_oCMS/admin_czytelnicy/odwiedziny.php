<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Lista odwiedzin <strong><?= $this->data['czytelnik']['imie']; ?> <?= $this->data['czytelnik']['nazwisko']; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<?php 
					$page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); 
				?>

				<?php if( $this->data['odwiedziny'] ) : ?>
					<table class="t-results">
						<tr>
							<th class='w10'>l.p.</th>
							<th class='w15'>Imię i nazwisko</th>
							<th class='w10'>Data odwiedzin</th>
							<th class='w35'>Miejsce odwiedzin</th>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['odwiedziny'] as $aData ) : ?>
						<?php $lp++; ?>
						<tr <?= $off; ?>>
							<td class='w10 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $lp; ?></td>
							<td class='w15'><?= $this->data['czytelnik']['imie']; ?> <?= $this->data['czytelnik']['nazwisko']; ?></td>
							<td class='w15'><?= $aData['data_wizyty']; ?></td>
							<td class='w35'><?= $aData['miejsce']; ?></td>
						</tr>
					<?php endforeach; ?>
					</table>
					<div>
						<a href="<?= BASE; ?>admin_czytelnicy" class="page-action-link">
							<i class="icon-wroc"></i> Wróć do listy czytelników
						</a>
					</div>
					<?= $page; ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
