<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Dodaj czytelnika</h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                    <div class="form-field">
	                        <label for="email">email</label>
	                        <input required="" name="email" id="email" value="<?= $this->data['email']; ?>" type="text">
	                    </div>
	                	<div class="form-field">
	                        <label for="imie">imię</label>
	                        <input required="" name="imie" id="imie" value="<?= $this->data['imie']; ?>" type="text">
	                    </div>
	                    <div class="form-field">
	                        <label for="nazwisko">nazwisko</label>
	                        <input required="" name="nazwisko" id="nazwisko" value="<?= $this->data['nazwisko']; ?>" type="text">
	                    </div>
						<div class="form-field">
	                        <label for="firma">firma</label>
	                        <input required="" name="firma" id="firma" value="<?= $this->data['firma']; ?>" type="text">
	                    </div>
	                    <div class="form-field">
	                        <label for="stanowisko">stanowisko</label>
	                        <input required="" name="stanowisko" id="stanowisko" value="<?= $this->data['stanowisko']; ?>" type="text">
	                    </div>
						
	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz jesli chcesz aktywowac konto czytelnika
	                        </label>
	                    </div>
						<br><br>
	                    <div><a href="<?= BASE; ?>admin_czytelnicy/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy czytelników</a> <button name="add" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>