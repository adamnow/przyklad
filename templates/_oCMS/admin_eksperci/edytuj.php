<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj eksperta: <strong><?= $this->data['eksperci']['imie']; ?> <?= $this->data['eksperci']['nazwisko']; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="imie">imie</label>
	                        <input name="imie" id="imie" required value="<?= $this->data['eksperci']['imie']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="nazwisko">nazwisko</label>
	                        <input name="nazwisko" id="nazwisko" required value="<?= $this->data['eksperci']['nazwisko']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="stanowisko">stanowisko</label>
	                        <input name="stanowisko" id="stanowisko" value="<?= $this->data['eksperci']['stanowisko']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="firma">firma</label>
	                        <input name="firma" id="firma" value="<?= $this->data['eksperci']['firma']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="bio">krótkie bio (max. 170 znaków) <span class='strlen'></span></label>
	                        <textarea name="bio" id="bio" maxlength='170'><?= $this->data['eksperci']['bio']; ?></textarea>
	                    </div>
	                    
	                    <?php /*
						<div class="form-field">
	                        <label for="opis_pl">zajawka</label>
	                        <textarea class='wyswig' name="zajawka_pl" id="zajawka_pl"><?= $this->data['eksperci']['zajawka_pl']; ?></textarea>
	                    </div>
	                    */ ?>

	                    <div class="form-field">
	                        <label for="opis_pl">opis</label>
	                        <textarea class='wyswig' name="opis_pl" id="opis_pl"><?= $this->data['eksperci']['tresc_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['eksperci']['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz aby aktywowac wpis
	                        </label>
	                    </div>
	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj zdjęcie w formacie jpg lub png o wymiarach 360x400px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/eksperci/' . $this->data['eksperci']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/eksperci/<?= $this->data['eksperci']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    	</div>
	                    </div>


						<br><br>
	                    <div><a href="<?= BASE; ?>admin_eksperci/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy ekspertów</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>