<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Dodaj nową firmę</strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-10">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="nazwa_pl">nazwa firmy</label>
	                        <input name="nazwa_pl" id="nazwa_pl" required value="<?= $this->data['nazwa_pl']; ?>" type="text">
	                    </div>
						
						<?php if( $this->data['categories'] ) : ?>
		                    <div class="form-field">
		                        	<label for="cat">kategoria firmy</label>
		                        	<select name="cat" id="cat" required="">
		                        		<option value="">-- wybierz kategorię firmy --</option>
		                        		<?php foreach( $this->data['categories'] as $aData ) : ?>
		                        			<?php $select = ( $this->data['kategoria'] == $aData['id'] ) ? 'selected' : ''; ?>
		                        			<option value="<?= $aData['id']; ?>" <?= $select; ?>><?= $aData['nazwa_' . LANG]; ?></option>
		                        		<?php endforeach; ?>
		                        	</select>
		                        <span class="dropdown"><i class="icon-angle-down"></i></span>
		                    </div>
						<?php endif; ?>
						<div class="form-field">
                        	<label for="wojewodztwo">województwo</label>
                        	<select name="wojewodztwo" id="wojewodztwo" required="">
                        		<option value="">-- wybierz województwo --</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'dolnośląskie' ) echo 'selected'; ?> value="dolnośląskie">dolnośląskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'kujawsko-pomorskie' ) echo 'selected'; ?> value="kujawsko-pomorskie">kujawsko-pomorskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'lubelskie' ) echo 'selected'; ?> value="lubelskie">lubelskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'lubuskie' ) echo 'selected'; ?> value="lubuskie">lubuskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'łódzkie' ) echo 'selected'; ?> value="łódzkie">łódzkie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'małopolskie' ) echo 'selected'; ?> value="małopolskie">małopolskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'mazowieckie' ) echo 'selected'; ?> value="mazowieckie">mazowieckie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'opolskie' ) echo 'selected'; ?> value="opolskie">opolskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'podkarpackie' ) echo 'selected'; ?> value="podkarpackie">podkarpackie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'podlaskie' ) echo 'selected'; ?> value="podlaskie">podlaskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'pomorskie' ) echo 'selected'; ?> value="pomorskie">pomorskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'śląskie' ) echo 'selected'; ?> value="śląskie">śląskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'świętokrzyskie' ) echo 'selected'; ?> value="świętokrzyskie">świętokrzyskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'warmińsko-mazurskie' ) echo 'selected'; ?> value="warmińsko-mazurskie">warmińsko-mazurskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'wielkopolskie' ) echo 'selected'; ?> value="wielkopolskie">wielkopolskie</option>
                        		<option <?php if( $this->data['wojewodztwo'] == 'zachodniopomorskie' ) echo 'selected'; ?> value="zachodniopomorskie">zachodniopomorskie</option>
                        	</select>
                        <span class="dropdown"><i class="icon-angle-down"></i></span>
		                </div>

	                    <div class="form-field">
	                        <label for="opis_pl">opis firmy</label>
	                        <textarea class='wyswig' name="opis_pl" id="opis_pl"><?= $this->data['opis_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                        <label for="adres">adres</label>
	                        <input name="adres" id="adres" required value="<?= $this->data['adres']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="kod">kod pocztowy</label>
	                        <input name="kod" id="kod" required value="<?= $this->data['kod']; ?>" type="text" class='short'>
	                    </div>

	                    <div class="form-field">
	                        <label for="miasto">miejscowosc</label>
	                        <input name="miasto" id="miasto" required value="<?= $this->data['miasto']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="email">email</label>
	                        <input name="email" id="email" value="<?= $this->data['email']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="telefon">telefon</label>
	                        <input name="telefon" id="telefon" value="<?= $this->data['telefon']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="fax">fax</label>
	                        <input name="fax" id="fax" value="<?= $this->data['fax']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="www">strona www</label>
	                        <input name="www" id="www" value="<?= $this->data['www']; ?>" type="text">
	                    </div>
						
	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz aby aktywowac firmę
	                        </label>
	                    </div>
	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj zdjęcie w formacie jpg lub png o szerokości minimum 400px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev"></div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_firmy/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy firm</a> <button name="add" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>