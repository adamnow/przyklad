<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj artykuł: <strong><?= $this->data['firmy']['nazwa_' . LANG]; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="nazwa_pl">nazwa firmy</label>
	                        <input name="nazwa_pl" id="nazwa_pl" required value="<?= htmlentities( $this->data['firmy']['nazwa_pl'] ); ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="www">strona www</label>
	                        <input name="www" id="www" value="<?= $this->data['firmy']['www']; ?>" type="text">
	                    </div>
						
	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['firmy']['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz aby aktywowac firmę
	                        </label>
	                    </div>
	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj zdjęcie w formacie jpg lub png o szerokości minimum 400px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/partnerzy/' . $this->data['firmy']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/partnerzy/<?= $this->data['firmy']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_partnerzy/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy partnerów</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>