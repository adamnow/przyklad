<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Lista reklam</h1>
		<div class="row">
			<div class="wctn row">
				<a href="<?= BASE; ?>admin_reklama/dodaj" class="or-btn or-link" value="1">Dodaj reklamę boczną</a>
				<a href="<?= BASE; ?>admin_reklama/dodaj_top" class="or-btn or-link" value="1">Dodaj reklamę górną</a>
				<br><br>
				<?php 
					$page = Helper::paginationCurrent( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); 
				?>
				<?php require "libs/templates/_elements/_admin_search.php"; ?>
				<div class="tabs">
					<a href="<?= BASE . Routing::$routing['controller']; ?>" <?php if( Routing::$routing['action'] == 'widok' ) echo "class='act'"; ?>>Boczne reklamy</a>
					<a href="<?= BASE . Routing::$routing['controller']; ?>/widok_top" <?php if( Routing::$routing['action'] == 'widok_top' ) echo "class='act'"; ?>>Reklamy na górze strony</a>
				</div>
				<?php if( $this->data['reklama'] ) : ?>
					<table class="t-results">
						<tr>
							<th class='w5'>Pozycja</th>
							<th class='w20'>Szczegóły</th>
							<th class='w20'>Tytuł</th>
							<th class='w15'>Data startu</th>
							<th class='w15'>Data zakończenia</th>
							<th class='w15 center-a'>Obraz</th>
							<th class='w15 center-a'>Operacje</th>
							<?pph /* <th class='w10 center-a'>Pozycja</th> */ ?>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['reklama'] as $aData ) : ?>
						<?php $lp++; ?>
						<?php $off = ( $aData['stat'] != 1 || $aData['_ACT_'] != 1 ) ? "class='toff'" : ""; ?>
						<tr <?= $off; ?>>
							<td class='w5 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $aData['pozycja']; ?></td>
							<td class='w20'>
								<div class="det-item">
									<em>dodający: <strong><?= $aData['user']['imie']; ?> <?= $aData['user']['nazwisko']; ?></em></strong><br>
									<em>data dodania: <strong><?= $aData['data_dodania']; ?></em></strong><br>
									<em>wyswietlenia: <strong><?= $aData['wyswietlenia']; ?></em></strong><br>
									<em>klikniecia: <strong><?= $aData['klikniecia']; ?></em></strong><br>
								</div>
							</td>
							<td class='w20'>
								<?php $s = $this->data['search']; ?>
								<?= $aData['tytul_' . LANG] = ( $this->data['search'] ) ? preg_replace( "/($s)/iu" , "<span class='search_highlight'>$1</span>", $aData['tytul_' . LANG] ) : $aData['tytul_' . LANG]; ?>
							</td>
							<td class='w15'><?= $aData['data_start']; ?></td>
							<td class='w15'><?= $aData['data_stop']; ?></td>
							<td class='w15 center-a'>
								<?php if( @file_get_contents( 'userfiles/images/rekl/' . $aData['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/rekl/<?= $aData['image']; ?>?<?= rand(0,25000); ?>" alt="" width='100'>
								<?php endif; ?>
							</td>
							<td class='w15 center-a'>
								<a href="<?= BASE . Routing::$routing['controller']; ?>/edytuj_top/<?= $aData['id']; ?>" class='show-link'><i class="icon-pencil"></i> edytuj</a>
								<?php if( $aData['stat'] == 1 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wylacz/<?= $aData['id']; ?>/top" class='show-link'><i class="icon-eye-off"></i> wyłącz</a>
								<?php else : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wlacz/<?= $aData['id']; ?>/top" class='show-link'><i class="icon-eye"></i> włącz</a>
								<?php endif; ?>
								<?php if( !Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/usun/<?= $aData['id']; ?>" data-message='Czy na pewno chcesz usunąć wpis?' data-tab='admin_reklama' data-i='<?= $aData['id']; ?>' class='show-link show-link-off'><i class="icon-trash"></i> usuń</a>
								<?php endif; ?>
							</td>
							<?php /*
							<td class='w10 center-a'>
								<?php if( $lp == 1 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/dol/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-down-open"></i></a>
								<?php elseif( $lp == $this->data['TOTAL'] ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/gora/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-up-open"></i></a>
								<?php else : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/gora/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-up-open"></i></a>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/dol/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-down-open"></i></a>
								<?php endif; ?>
							</td>
							*/ ?>
						</tr>
					<?php endforeach; ?>
					</table>
					<?= $page; ?>
				<?php else : ?>
					<?php require "libs/templates/_elements/_no_data.php"; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
