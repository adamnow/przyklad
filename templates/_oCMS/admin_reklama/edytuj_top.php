<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj reklamę: <strong><?= $this->data['reklama']['tytul_pl']; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="tytul_pl">tutuł reklamy</label>
	                        <input name="tytul_pl" id="tytul_pl" required value="<?= htmlentities( $this->data['reklama']['tytul_pl'] ); ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="link">link docelowy (z http://)</label>
	                        <input name="link" id="link" required value="<?= $this->data['reklama']['link']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="data_start">data rozpoczęcia</label>
	                        <input name="data_start" id="data_start" class='date short' required value="<?= $this->data['reklama']['data_start']; ?>" type="text">
	                    </div>
	                    
						<div class="form-field">
	                        <label for="godzina_start">godzina rozpoczęcia (GG:MM)</label>
	                        <input name="godzina_start" id="godzina_start" class='short' value="<?= $this->data['reklama']['godzina_start']; ?>" required type="text">
	                    </div>

						<div class="form-field">
	                        <label for="data_stop">data zakończenia</label>
	                        <input name="data_stop" id="data_stop" class='date short' required value="<?= $this->data['reklama']['data_stop']; ?>" type="text">
	                    </div>
	                    
						<div class="form-field">
	                        <label for="godzina_stop">godzina zakończenia (GG:MM)</label>
	                        <input name="godzina_stop" id="godzina_stop" class='short' value="<?= $this->data['reklama']['godzina_stop']; ?>" required type="text">
	                    </div>

	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['reklama']['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz aby aktywowac wpis
	                        </label>
	                    </div>

	                    <div class="form-field">
	                    	<div id="image-error"></div>
	                    	<div id="image-prev"></div>
	                    </div>

	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Zmień zdjęcie w formacie jpg lub png o wymiarach 750x100px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/rekl/' . $this->data['reklama']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/rekl/<?= $this->data['reklama']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_reklama/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy reklam</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>