<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Dodaj nową kategorię artykułu</strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-10">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="nazwa_pl">nazwa kategorii</label>
	                        <input required="" name="nazwa_pl" id="nazwa_pl" value="<?= $this->data['nazwa_pl']; ?>" type="text">
	                    </div>

	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Aktywuj kategorię
	                        </label>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_kategorie/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy kategorii</a> <button name="add" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>