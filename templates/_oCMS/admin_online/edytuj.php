<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj artykuł: <strong><?= html_entity_decode( $this->data['online']['tytul_pl'] ); ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="tytul_pl">tutuł</label>
	                        <input name="tytul_pl" id="tytul_pl" required value="<?= htmlentities( $this->data['online']['tytul_pl'] ); ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="numer">numer wydania</label>
	                        <input name="numer" id="numer" required value="<?= $this->data['online']['numer']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="data_publikacji">data publikacji</label>
	                        <input name="data_publikacji" id="data_publikacji" class="date short" required="" value="<?= $this->data['online']['_DP_']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="godzina_publikacji">godzina publikacji (GG:MM)</label>
	                        <input name="godzina_publikacji" id="godzina_publikacji" class="short" value="<?= $this->data['online']['_HP_']; ?>" required="" type="text">
	                    </div>
	                    
						<div class="form-field">
	                        <label for="opis_pl">zajawka</label>
	                        <textarea class='wyswig' name="zajawka_pl" id="zajawka_pl"><?= $this->data['online']['zajawka_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                        <label for="opis_pl">opis</label>
	                        <textarea class='wyswig' name="opis_pl" id="opis_pl"><?= $this->data['online']['tresc_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                        <!-- <label for="issue_link">link do issue</label> -->
	                        <label for="issue_link">numer issue</label>
	                        <input name="issue_link" id="issue_link" required value="<?= $this->data['online']['issue_link']; ?>" type="text">
	                    </div>

	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['online']['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> zaznacz aby aktywowac wpis
	                        </label>
	                    </div>
	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj okładkę czasopisma w formacie 460x600px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/online/' . $this->data['online']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/online/<?= $this->data['online']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_online/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy numerów</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>