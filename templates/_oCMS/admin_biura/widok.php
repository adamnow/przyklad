<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Lista biur prasowych</h1>
		<div class="row">
			<div class="wctn row">
				<a href="<?= BASE; ?>admin_biura/dodaj" class="or-btn or-link" value="1">Dodaj biuro</a>
				<br><br>
				<?php 
					$page = Helper::paginationCurrent( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); 
				?>
				<?php if( $this->data['biura'] ) : ?>
					<?php require "libs/templates/_elements/_admin_search.php"; ?>
					<table class="t-results">
						<tr>
							<th class='w5'>l.p.</th>
							<th class='w25'>Szczegóły</th>
							<th class='w10'>Firma</th>
							<th class='w15'>Adres</th>
							<th class='w20'>Opis</th>
							<th class='w10 center-a'>Administratorzy</th>
							<th class='w10 center-a'>Operacje</th>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['biura'] as $aData ) : ?>
						<?php $lp++; ?>
						<?php $off = ( $aData['stat'] != 1 ) ? "class='toff'" : ""; ?>
						<tr <?= $off; ?>>
							<td class='w5 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $lp; ?></td>
							<td class='w25'>
								<div class="det-item">
									<em>data dodania: <strong><?= $aData['data_dodania']; ?></strong></em><br>
									<em>adres: <strong><?= $aData['adres']; ?>, <?= $aData['kod']; ?> <?= $aData['miasto']; ?></strong></em><br>
									<em>email: <strong><?= $aData['email']; ?></strong></em><br>
									<em>www: <strong><?= $aData['www']; ?></strong></em><br>
									<em>telefon: <strong><?= $aData['telefon']; ?></strong></em><br>
								</div>
							</td>
							<td class='w10'><?= $aData['firma']; ?></td>
							<td class='w15'><?= $aData['adres']; ?>, <?= $aData['kod']; ?> <?= $aData['miasto']; ?></td>
							<td class='w20'><?= strip_tags( html_entity_decode( $aData['opis'] ) ); ?></td>
							<td class='w10 center-a'><a href="<?= BASE; ?>admin_users/widok/<?= $aData['id']; ?>">zobacz</a></td>
							<td class='w10 center-a'>
								<a href="<?= BASE . Routing::$routing['controller']; ?>/edytuj/<?= $aData['id']; ?>" class='show-link'><i class="icon-pencil"></i> edytuj</a>
								<?php if( $aData['stat'] == 1 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wylacz/<?= $aData['id']; ?>" class='show-link'><i class="icon-eye-off"></i> wyłącz</a>
								<?php else : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wlacz/<?= $aData['id']; ?>" class='show-link'><i class="icon-eye"></i> włącz</a>
								<?php endif; ?>
								<a href="<?= BASE . Routing::$routing['controller']; ?>/usun/<?= $aData['id']; ?>" data-message='Uwaga! Usunięcie biura będzie skutkowało usunięciem artykułów wprowadzonych przez to biuro. Czy na pewno chcesz to zrobic?' data-tab='admin_biura' data-i='<?= $aData['id']; ?>' class='show-link show-link-off'><i class="icon-trash"></i> usuń</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</table>
					
					<?= $page; ?>
				<?php else : ?>
					<?php require "libs/templates/_elements/_no_data.php"; ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
