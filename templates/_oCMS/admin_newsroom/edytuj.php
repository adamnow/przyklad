<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj artykuł: <strong><?= $this->data['artykul']['tytul_pl']; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
						<?php if( Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) : ?>
							<input type="hidden" name='corp' value='<?= $this->data['artykul']['id_biura']; ?>'>
						<?php else : ?>
							<div class="form-field">
	                        	<label for="corp">biuro prasowe</label>
	                        	<select name="corp" id="corp" required>
	                        		<option value="">-- wybierz biuro prasowe --</option>
	                        		<?php if( $this->data['biuro'] ) : ?>
	                        			<?php foreach( $this->data['biuro'] as $aData ) : ?>
	                        				<?php $select = ( $aData['id'] == $this->data['artykul']['id_biura'] ) ? 'selected' : ''; ?>
	                        				<option value="<?= $aData['id']; ?>" <?= $select; ?>><?= $aData['nazwa_pl']; ?></option>
	                        			<?php endforeach; ?>
	                        		<?php endif; ?>
	                        	</select>
	                        	<span class="dropdown"><i class="icon-angle-down"></i></span>
	                    	</div>
						<?php endif; ?>
	                	<div class="form-field">
	                        <label for="tytul_pl">tutuł</label>
	                        <input name="tytul_pl" id="tytul_pl" required value="<?= htmlentities( $this->data['artykul']['tytul_pl'] ); ?>" type="text">
	                    </div>
	                    
						<div class="form-field">
	                        <label for="opis_pl">zajawka</label>
	                        <textarea class='wyswig' name="zajawka_pl" id="zajawka_pl"><?= $this->data['artykul']['zajawka_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                        <label for="opis_pl">opis</label>
	                        <textarea class='wyswig' name="opis_pl" id="opis_pl"><?= $this->data['artykul']['tresc_pl']; ?></textarea>
	                    </div>
						
						<?php if( $this->data['categories'] ) : ?>
								<div class="form-field form-categories">
			                        <label for="">kategorie artykułu</label>
									<?php foreach( $this->data['categories'] as $aData ) : ?>
				                        <div class="checkbox-btn">
				                        	<label for='c<?= $aData['id']; ?>'>
				                        		<?php $checked = ( in_array( $aData['id'] , $this->data['ca'] ) ) ? 'checked' : ''; ?>
				                        		<input type="checkbox" name='kategoria[]' value='<?= $aData['id']; ?>' id='c<?= $aData['id']; ?>' <?= $checked; ?>>
				                        		<span><?= $aData['nazwa_' . LANG]; ?></span>
				                        	</label>
				                        </div>
									<?php endforeach; ?>
			                    </div>
						<?php endif; ?>

						<div class="form-field">
	                        <label for="zrodlo">źródło artykułu</label>
	                        <input name="zrodlo" id="zrodlo" value="<?= $this->data['artykul']['zrodlo']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="tagi">tagi (każdy tag oddzielony przecinkiem lub srednikiem)</label>
	                        <textarea name="tagi" id="tagi"><?= $this->data['artykul']['tagi']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj zdjęcie w formacie jpg lub png o szerokości minimum 750px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/newsroom/' . $this->data['artykul']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/newsroom/<?= $this->data['artykul']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_newsroom/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy artykułów</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>