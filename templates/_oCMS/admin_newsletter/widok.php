<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Lista adresów Newslettera</h1>
		<div class="row">
			<div class="wctn row">
				<a href="<?= BASE; ?>admin_newsletter/dodaj" class="or-btn or-link" value="1">Dodaj adres do newslettera</a>
				<?php if( $this->data['newsletter'] ) : ?>
					<a href="<?= BASE; ?>admin_newsletter/xls" class="or-btn or-link xls" value="1"><img src="<?= BASE; ?>public/images/xls-icon.svg" alt="">Eksportuj</a>
				<?php endif; ?>
				<?php 
					$page = Helper::paginationCurrent( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); 
				?>

				<?php if( $this->data['newsletter'] ) : ?>
					<?php require "libs/templates/_elements/_admin_search.php"; ?>
					<table class="t-results">
						<tr>
							<th class='w5'>l.p.</th>
							<th class='w20'>Email</th>
							<th class='w20'>Data zapisu <br>data aktywacji</th>
							<th class='w10 center-a'>Operacje</th>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['newsletter'] as $aData ) : ?>
						<?php $lp++; ?>
						<?php $off = ( $aData['stat'] != 1 ) ? "class='toff'" : ""; ?>
						<tr <?= $off; ?>>
							<td class='w5 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $lp; ?></td>
							<td class='w20'><?= $aData['email']; ?></td>
							<td class='w20'><?= $aData['data_zapisu']; ?><br><?= $aData['data_potwierdzenia']; ?></td>
							<td class='w10 center-a'>
								<a href="<?= BASE . Routing::$routing['controller']; ?>/edytuj/<?= $aData['id']; ?>" class='show-link'><i class="icon-pencil"></i> edytuj</a>
								<?php if( $aData['stat'] == 1 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wylacz/<?= $aData['id']; ?>" class='show-link'><i class="icon-eye-off"></i> wypisz</a>
								<?php else : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/wlacz/<?= $aData['id']; ?>" class='show-link'><i class="icon-eye"></i> zapisz</a>
								<?php endif; ?>
								<?php if( !Auth::accessDenied( $_SESSION[AUTH_SESSION_NAME]['permissions'], ['newsroom', 'user'] ) ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/usun/<?= $aData['id']; ?>" data-message='Czy na pewno chcesz usunąć adres email z bazy Newsletter?' data-tab='admin_newsletter' data-i='<?= $aData['id']; ?>' class='show-link show-link-off'><i class="icon-trash"></i> usuń</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</table>
					<?= $page; ?>
				<?php else : ?>
					<?php require "libs/templates/_elements/_no_data.php"; ?>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>
