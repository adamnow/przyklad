<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Slider</h1>
		<div class="row">
			<div class="wctn row">
				<?php if( $this->data['slider'] ) : ?>
					<h3 class='help-info'>Elementy slidera (max <?= SLIDER_ELEMENTS; ?>)</h3>
					<table class="t-results">
						<tr>
							<th class='w5'>Pozycja slider</th>
							<th class='w20'>Szczegóły</th>
							<th class='w20'>Tytuł</th>
							<th class='w25'>Zajawka</th>
							<th class='w10 center-a'>Obraz</th>
							<th class='w15 center-a'>Operacje</th>
							<th class='w5 center-a'>Pozycja</th>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['slider'] as $aData ) : ?>
						<?php $lp++; ?>
						<?php $off = ( $aData['stat'] != 1 ) ? "class='toff'" : ""; ?>
						<tr <?= $off; ?>>
							<td class='w5 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $aData['slider_pozycja']; ?></td>
							<td class='w20'>
								<div class="det-item">
									<em>dodający: <strong><?= $aData['user']['imie']; ?> <?= $aData['user']['nazwisko']; ?></em></strong><br>
									<em>data dodania: <strong><?= $aData['data_dodania']; ?></em></strong><br>
									<em>wyswietlenia: <strong><?= $aData['wyswietlenia']; ?></em></strong><br>
									<em>dostęp: <strong><?= $aData['ograniczony_dostep'] = ( $aData['ograniczony_dostep'] == 1 ) ? 'Ograniczony' : 'Nieograniczony'; ?></em></strong><br>
									<em>slider: <strong><?= $aData['slider'] = ( $aData['slider'] == 1 ) ? 'Tak' : 'Nie'; ?></em></strong><br>
									<em>pod sliderem: <strong><?= $aData['sub_slider'] = ( $aData['sub_slider'] == 1 ) ? 'Tak' : 'Nie'; ?></em></strong>
								</div>
							</td>
							<td class='w20'><?= $aData['tytul_' . LANG]; ?></td>
							<td class='w25'><?= mb_substr( $aData['zajawka_' . LANG] , 0, 150, 'utf-8' ); ?></td>
							<td class='w10 center-a'>
								<?php if( @file_get_contents( 'userfiles/images/artykuly/' . $aData['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/artykuly/<?= $aData['image']; ?>?<?= rand(0,25000); ?>" alt="" width='100'>
								<?php endif; ?>
							</td>
							<td class='w15 center-a'>
								<a href="<?= BASE . Routing::$routing['controller']; ?>/usun/<?= $aData['id']; ?>" data-message='Czy na pewno chcesz usunąć wpis ze slidera?' data-tab='admin_slider' data-i='<?= $aData['id']; ?>' class='show-link show-link-off'><i class="icon-trash"></i> usuń ze slidera</a>
							</td>
							<td class='w5 center-a'>
								<?php if( $lp == 1 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/dol/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-down-open"></i></a>
								<?php elseif( $lp == $this->data['slider_count']['TOTAL'] || $lp == SLIDER_ELEMENTS ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/gora/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-up-open"></i></a>
								<?php else : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/gora/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-up-open"></i></a>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/dol/<?= $aData['id']; ?>" data-i='<?= $aData['id']; ?>' class='show-link'><i class="icon-down-open"></i></a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</table>
				<?php endif; ?>

				<?php if( !$this->data['limit_slides'] ) : ?>
					<?php 
						$sec_off = "off-section";  
						$limitSlider = "<h5>Nie możesz dodac kolejnego elementu do slidera. Dopuszczalna liczba slajdów to " . SLIDER_ELEMENTS . ".</h5>";
					?>
				<?php endif; ?>

				<div class="slider-add-elem <?= $sec_off; ?>">
					
					<?= $limitSlider; ?>
					
					<?php if( $this->data['artykuly'] ) : ?>
						<h3 class='help-info'>Dodaj wpis do slidera</h3>
						<?php require "libs/templates/_elements/_admin_search.php"; ?>
						<table class="t-results">
							<tr>
								<th class='w5'>Pozycja slider</th>
								<th class='w20'>Szczegóły</th>
								<th class='w20'>Tytuł</th>
								<th class='w25'>Zajawka</th>
								<th class='w10 center-a'>Obraz</th>
								<th class='w20 center-a'>Operacje</th>
							</tr>
						<?php 
							$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
							$lp *= PERPAGE;
						?>
						<?php foreach( $this->data['artykuly'] as $aData ) : ?>
							<?php $lp++; ?>
							<?php $off = ( $aData['stat'] != 1 ) ? "class='toff'" : ""; ?>
							<tr <?= $off; ?>>
								<td class='w5 rel'><i class="icon-eye-off" title='Wyłączone'></i><?= $aData['slider_pozycja']; ?></td>
								<td class='w20'>
									<div class="det-item">
										<em>dodający: <strong><?= $aData['user']['imie']; ?> <?= $aData['user']['nazwisko']; ?></em></strong><br>
										<em>data dodania: <strong><?= $aData['data_dodania']; ?></em></strong><br>
										<em>wyswietlenia: <strong><?= $aData['wyswietlenia']; ?></em></strong><br>
										<em>dostęp: <strong><?= $aData['ograniczony_dostep'] = ( $aData['ograniczony_dostep'] == 1 ) ? 'Ograniczony' : 'Nieograniczony'; ?></em></strong><br>
										<em>slider: <strong><?= $aData['slider'] = ( $aData['slider'] == 1 ) ? 'Tak' : 'Nie'; ?></em></strong><br>
										<em>pod sliderem: <strong><?= $aData['sub_slider'] = ( $aData['sub_slider'] == 1 ) ? 'Tak' : 'Nie'; ?></em></strong>
									</div>
								</td>
								<td class='w20'><?= $aData['tytul_' . LANG]; ?></td>
								<td class='w25'><?= mb_substr( $aData['zajawka_' . LANG] , 0, 150, 'utf-8' ); ?></td>
								<td class='w10 center-a'>
									<?php if( @file_get_contents( 'userfiles/images/artykuly/' . $aData['image'] ) ) : ?>
										<img src="<?= BASE; ?>userfiles/images/artykuly/<?= $aData['image']; ?>?<?= rand(0,25000); ?>" alt="" width='100'>
									<?php endif; ?>
								</td>
								<td class='w20 center-a'>
									<?php if( $this->data['limit_slides'] ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/dodaj/<?= $aData['id']; ?>" class='show-link'><i class="icon-doc-add"></i> dodaj do slidera</a>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</table>
					<?php else : ?>
						<h5>Brak nowych elementów do dodania do slidera</h5>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</div>
