<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Lista nowych zamówień prenumeraty</h1>
		<div class="row">
			<div class="wctn row">
				<a href="<?= BASE; ?>admin_prenumerata/nowe" class="or-btn or-link" value="1">Zobacz nowe</a>
				<a href="<?= BASE; ?>admin_prenumerata/dodaj" class="or-btn or-link" value="1">Dodaj prenumeratora</a>
				<a href="<?= BASE; ?>admin_prenumerata/xls" class="or-btn or-link xls" value="1"><img src="<?= BASE; ?>public/images/xls-icon.svg" alt=""> Eksportuj</a>

				<br><br>
				<?php 
					$page = Helper::paginationCurrent( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); 
				?>
				<?php $this->data['prenumerata_']; ?>
				<?php if( $this->data['prenumerata_'] ) : ?>
					<?php require "libs/templates/_elements/_admin_search.php"; ?>
					<table class="t-results">
						<tr>
							<th class='w5'>l.p.</th>
							<th class='w15'>Szczegóły</th>
							<th class='w20'>Firma</th>
							<th class='w20'>Wysyłka</th>
							<th class='w10'>Prenumerata</th>
							<th class='w10'>Okres</th>
							<th class='w10'>Ilosc</th>
							<th class='w10 center-a'>Operacje</th>
							<?php /* <th class='w10 center-a'>Pozycja</th> */ ?>
						</tr>
					<?php 
						$lp = ( $_GET['p'] ) ? ( (int)$_GET['p'] - 1 ) : 0;
						$lp *= PERPAGE;
					?>
					<?php foreach( $this->data['prenumerata_'] as $aData ) : ?>
						<?php $lp++; ?>
						<?php 
							$off = ( $aData['stat'] != 1 ) ? "class='toff'" : ""; 
							$new = ( $aData['nowe'] == 1 ) ? true : false; 
							$confirm = ( $aData['stat'] == 1 ) ? true : false; 
						?>
						<tr>
							<td class='w5 rel'>
								<?php if ( $confirm ) : ?>
									<i class="icon-ok oki"></i>
								<?php endif; ?>
								<?= $lp; ?>
							</td>
							<td class='w15'>
								<div class="det-item">
									<em>data dodania: <strong><?= $aData['data_dodania']; ?></em></strong><br>
									<em>status: <strong><?= $stat = ( $aData['stat'] == 1 ) ? "Potwierdzone" : "Niepotwierdzone"; ?></em></strong><br>
								</div>
							</td>
							<td class='w20'>
								<?= $aData['firma']; ?><br>
								<?= $aData['adres']; ?><br>
								<?= $aData['kod_miejscowosc']; ?><br>
								NIP: <?= $aData['nip']; ?><br>
								email: <?= $aData['email']; ?><br>
								telefon: <?= $aData['telefon']; ?><br>
							</td>
							<td class='w20'>
								<?= $aData['wysylka_firma']; ?><br>
								<?= $aData['wysylka_adres']; ?><br>
								<?= $aData['wysylka_kod_miejscowosc']; ?><br>
							</td>
							<td class='w10'><?= $aData['prenumerata']; ?></td>
							<td class='w10'><?= $aData['okres']; ?></td>
							<td class='w10'><?= $aData['egzemplarze']; ?></td>
							<td class='w10 center-a'>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/edytuj/<?= $aData['id']; ?>" class='show-link'><i class="icon-pencil"></i> edytuj</a>
								<?php if( $aData['stat'] == 0 ) : ?>
									<a href="<?= BASE . Routing::$routing['controller']; ?>/potwierdz/<?= $aData['id']; ?>/<?= Routing::$routing['action']; ?>" class='show-link show-link-off' data-message="Czy na pewno chcesz aktywować prenumeratę?" data-action='potwierdz' data-tab="admin_prenumerata" data-i="<?= $aData['id']; ?>"><i class="icon-pencil"></i> potwierdź</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</table>
					<?= $page; ?>
				<?php else : ?>
					<?php require "libs/templates/_elements/_no_data.php"; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
