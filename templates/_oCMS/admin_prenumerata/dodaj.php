<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Dodaj nowego prenumeratora</strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-10">

					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
						<div class="form-field">
                        	<label for="prenumerata">prenumerata</label>
                        	<select name="prenumerata" id="prenumerata" required="">
                        		<option value="">-- wybierz prenumeratę --</option>
                        		<option value="wersja papierowa" <?php if( $this->data['prenumerata'] == 'wersja papierowa' ) echo 'selected'; ?>>wersja papierowa</option>
                        		<option value="wersja papierowa dla członków PISA" <?php if( $this->data['prenumerata'] == 'wersja papierowa dla członków PISA' ) echo 'selected'; ?>>wersja papierowa dla członków PISA</option>
                        		<option value="wersja elektroniczna" <?php if( $this->data['prenumerata'] == 'wersja elektroniczna' ) echo 'selected'; ?>>wersja elektroniczna</option>
                        	</select>
                        	<span class="dropdown"><i class="icon-angle-down"></i></span>
	                    </div>

	                    <div class="form-field">
                        	<label for="okres">okres prenumeraty</label>
                        	<select name="okres" id="okres" required="">
                        		<option value="">-- wybierz okres prenumeraty --</option>
                        		<option value="roczna" <?php if( $this->data['okres'] == 'roczna' ) echo 'selected'; ?>>Prenumerata roczna</option>
                        		<option value="odnawialna" <?php if( $this->data['okres'] == 'odnawialna' ) echo 'selected'; ?>>Prenumerata odnawialna</option>
                        	</select>
                        	<span class="dropdown"><i class="icon-angle-down"></i></span>
	                    </div>

	                	<div class="form-field">
	                        <label for="ilosc">liczba egzemplarzy</label>
	                        <input name="ilosc" id="ilosc" required value="<?= $this->data['ilosc']; ?>" type="text">
	                    </div>

	                    <h4>DANE DO FAKTURY</h4>

	                    <div class="form-field">
	                        <label for="firma">firma</label>
	                        <input name="firma" id="firma" required value="<?= $this->data['firma']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="adres">adres (ulica, nr domu, nr lokalu)</label>
	                        <input name="adres" id="adres" required value="<?= $this->data['adres']; ?>" type="text">
	                    </div>
	                    
	                    <div class="form-field">
	                        <label for="miejscowosc">kod, miejscowosc</label>
	                        <input name="miejscowosc" id="miejscowosc" required value="<?= $this->data['kod_miejscowosc']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="nip">nip</label>
	                        <input name="nip" id="nip" required value="<?= $this->data['nip']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="email">email</label>
	                        <input name="email" id="email" required value="<?= $this->data['email']; ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="telefon">telefon</label>
	                        <input name="telefon" id="telefon" required value="<?= $this->data['telefon']; ?>" type="text">
	                    </div>

	                    <h4>DANE DO WYSYŁKI</h4>
	                    <div class="form-field form-field-checkbox">
	                        <label for="inny">
	                        	<?php $checked = ( $this->data['inny'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='inny' id='inny' value='1' <?= $checked; ?>> inny niż dane do faktury
	                        </label>
	                    </div>

	                    <div class="form-field other-disabled">
	                        <label for="firma_inny">firma / imię i nazwisko</label>
	                        <input name="firma_inny" id="firma_inny" disabled value="<?= $this->data['firma_inny']; ?>" type="text">
	                    </div>

	                    <div class="form-field other-disabled">
	                        <label for="adres_inny">adres (ulica, nr domu, nr lokalu)</label>
	                        <input name="adres_inny" id="adres_inny" disabled value="<?= $this->data['adres_inny']; ?>" type="text">
	                    </div>

	                    <div class="form-field other-disabled">
	                        <label for="miejscowosc_inny">kod, miejscowosc</label>
	                        <input name="miejscowosc_inny" id="miejscowosc_inny" disabled value="<?= $this->data['miejscowosc_inny']; ?>" type="text">
	                    </div>

						<?php /*
	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<input type="checkbox" name='stat' id='stat' value='1'> zaznacz aby aktywowac wpis
	                        </label>
	                    </div>
	                    */ ?>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_prenumerata/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy prenumeratorów</a> <button name="add" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>