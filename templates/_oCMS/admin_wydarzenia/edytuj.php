<?php require TEMPLATE_DIR . "_elements/_user.php"; ?>
<?php //require TEMPLATE_DIR . "_elements/_adminnav.php"; ?>

<?php
	Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
	Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
	Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
	Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>

<div class="content admin-content">
	<div class="ctn1360">
		<h1>Edytuj wydarzenie: <strong><?= $this->data['wydarzenie']['tytul_pl']; ?></strong></h1>
		<div class="row">
			<div class="wctn row">
				<div class="col-sm-8">
					<form method="post" class="uedit uadd form-ocms" enctype="multipart/form-data">
	                	<div class="form-field">
	                        <label for="tytul_pl">nazwa wydarzenia</label>
	                        <input name="tytul_pl" id="tytul_pl" required value="<?= htmlentities( $this->data['wydarzenie']['tytul_pl'] ); ?>" type="text">
	                    </div>

	                    <div class="form-field">
	                        <label for="data">data wydarzenia</label>
	                        <input name="data" id="data" class='date short' required value="<?= $this->data['wydarzenie']['data_wydarzenia']; ?>" type="text">
	                    </div>
	                    
						<div class="form-field">
	                        <label for="opis_pl">zajawka wzdarzenia</label>
	                        <textarea class='wyswig' name="zajawka_pl" id="zajawka_pl"><?= $this->data['wydarzenie']['zajawka_pl']; ?></textarea>
	                    </div>

	                    <div class="form-field">
	                        <label for="opis_pl">opis wzdarzenia</label>
	                        <textarea class='wyswig' name="opis_pl" id="opis_pl"><?= $this->data['wydarzenie']['tresc_pl']; ?></textarea>
	                    </div>
						<?php if( $this->data['categories'] ) : ?>
							<div class="form-field">
	                        	<label for="cat">kategoria wydarzenia</label>
	                        	<select name="cat" id="cat" required="">
	                        		<option value="">-- wybierz kategorię --</option>
									<?php foreach( $this->data['categories'] as $aData ) : ?>
										<?php $select = ( $this->data['wydarzenie']['id_kategorii'] == $aData['id'] ) ? 'selected' : ''; ?>
	                        			<option value="<?= $aData['id']; ?>" <?= $select; ?>><?= $aData['nazwa_' . LANG]; ?></option>
									<?php endforeach; ?>
	                        	</select>
	                        	<span class="dropdown"><i class="icon-angle-down"></i></span>
	                    	</div>
                    	<?php endif; ?>

	                    <div class="form-field">
	                        <label for="tagi">tagi (każdy tag oddzielony przecinkiem lub srednikiem)</label>
	                        <textarea name="tagi" id="tagi"><?= $this->data['wydarzenie']['tagi']; ?></textarea>
	                    </div>

	                    <div class="form-field form-field-checkbox">
	                        <label for="stat">
	                        	<?php $checked = ( $this->data['wydarzenie']['stat'] == 1 ) ? 'checked' : ''; ?>
	                        	<input type="checkbox" name='stat' id='stat' value='1' <?= $checked; ?>> Zaznacz aby aktywowac wpis
	                        </label>
	                    </div>
	                    <div class="form-field">
	                    	<div class="image-input">
	                    		<span>Dodaj zdjęcie w formacie jpg lub png o szerokości minimum 750px</span>
	                    		<input type="file" name='image_upload' id='image_upload' accept=".png, .jpg, .jpeg">
	                    	</div>
	                    	<div id="image-error"></div>
	                    	<div id="image-prev">
	                    		<?php if( @file_get_contents( 'userfiles/images/wydarzenia/' . $this->data['wydarzenie']['image'] ) ) : ?>
									<img src="<?= BASE; ?>userfiles/images/wydarzenia/<?= $this->data['wydarzenie']['image']; ?>?<?= rand(0,25000); ?>" alt="">
								<?php endif; ?>
	                    	</div>
	                    </div>

						<br><br>
	                    <div><a href="<?= BASE; ?>admin_wydarzenia/widok" class="page-action-link"><i class="icon-wroc"></i> Wróć do listy wydarzeń</a> <button name="edit" class="or-btn or-link" value="1">Zapisz</button></div>
	                </form>
                </div>

			</div>
		</div>
	</div>
</div>