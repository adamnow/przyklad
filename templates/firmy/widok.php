<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show companies">
        <?php if( $this->data['firmy'] ) : ?>
            <h1><?= $this->data['firmy']['nazwa_' . LANG]; ?> </h1>

            <div class="event-i corp-i">
                <figure class="col-sm-4">
                    <?php if( @file_get_contents( "userfiles/images/firmy/{$this->data['firmy']['image']}" ) ) : ?>
                        <img src="<?= BASE; ?>userfiles/images/firmy/<?= $this->data['firmy']['image']; ?>" alt="">
                    <?php else : ?>
                        <div class="no-image"></div>
                    <?php endif; ?>
                </figure>
                <div class="col-sm-8 e-txt">
                    <p class="e-title"><?= $this->data['firmy']['nazwa_' . LANG]; ?></p>
                    <div class="corp-detail-div">
                        <?php if( $this->data['firmy']['adres'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-home"></i>
                                <?= $this->data['firmy']['adres']; ?><br><?= $this->data['firmy']['kod']; ?> <?= $this->data['firmy']['miasto']; ?>
                            </span>
                        <?php endif; ?>
                        <br>
                        <?php if( $this->data['firmy']['telefon'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-phone"></i>
                                <a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $this->data['firmy']['telefon'] ); ?>" target='_blank'><?= $this->data['firmy']['telefon']; ?></a>
                            </span>
                        <?php endif; ?>
                        <?php if( $this->data['firmy']['email'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-mail"></i>
                                <a href="mailto:<?= $this->data['firmy']['email']; ?>" target='_blank'><?= $this->data['firmy']['email']; ?></a>
                            </span>
                        <?php endif; ?>
                        <?php if( $this->data['firmy']['fax'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-print"></i>
                                <?= $this->data['firmy']['fax']; ?>
                            </span>
                        <?php endif; ?>
                        <?php if( $this->data['firmy']['www'] ) : ?>
                            <span class='corp-ico'>
                                <i class="icon-monitor"></i>
                                <a href='<?php if( preg_match( "/^(http:\/\/)/", $this->data['firmy']['www'] ) ) { echo $this->data['firmy']['www']; } else { echo "http://" . $this->data['firmy']['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $this->data['firmy']['www'] ); ?></a>
                            </span>
                        <?php endif; ?>
                    </div>
<?php /* 
                    <table class="corp-datail">
                        <tbody>
                            <tr>
                                <td class='td-ico'><i class="icon-home"></i></td>
                                <td><?= $this->data['firmy']['adres']; ?><br><?= $this->data['firmy']['kod']; ?> <?= $this->data['firmy']['miasto']; ?></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class='td-ico'><i class="icon-phone"></i></td>
                                <td><a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $this->data['firmy']['telefon'] ); ?>" target='_blank'><?= $this->data['firmy']['telefon']; ?></a></td>
                                <td class='td-ico'><i class="icon-mail"></i></td>
                                <td>
                                    <a href="mailto:<?= $this->data['firmy']['email']; ?>" target='_blank'><?= $this->data['firmy']['email']; ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td class='td-ico'><i class="icon-print"></i></td>
                                <td><?= $this->data['firmy']['fax']; ?></td>
                                <td class='td-ico'><i class="icon-monitor"></i></td>
                                <td>
                                    <a href='<?php if( preg_match( "/^(http:\/\/)/", $this->data['firmy']['www'] ) ) { echo $this->data['firmy']['www']; } else { echo "http://" . $this->data['firmy']['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $this->data['firmy']['www'] ); ?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
*/ ?>
                </div>
            </div>
            <?php if( $this->data['firmy']['opis_' . LANG] ) : ?>
                <div class="event-i-det-c">
                    <?= $this->data['firmy']['opis_' . LANG]; ?>
                </div>
            <?php endif; ?>
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>
        
    </div>
    <!-- ! lewa strona -->
    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>