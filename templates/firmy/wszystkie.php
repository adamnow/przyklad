<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
    <div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show events companies">
        <h1><?= $this->data['dictionary'][57][LANG]; ?></h1>
        <span class="article-date">&nbsp;</span>
        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <?php if( $this->data['firmy'] ) : ?>
            <form class='search-c'>
                <input type="search" name='szukaj' placeholder='<?= $this->data['dictionary'][86][LANG]; ?>' value='<?= $this->data['search']; ?>'>
                <button><i class="icon-search"></i></button>
            </form>
            <?php foreach( $this->data['firmy'] as $aData ) : ?>

                <div class="event-i corp-i">
                    <figure class="col-sm-4">
                        <?php if( @file_get_contents( "userfiles/images/firmy/{$aData['image']}" ) ) : ?>
                            <img src="<?= BASE; ?>userfiles/images/firmy/<?= $aData['image']; ?>" alt="">
                        <?php else : ?>
                            <div class="no-image"></div>
                        <?php endif; ?>
                    </figure>
                    <div class="col-sm-8 e-txt">
                        <p class="e-title">
                            <?php if( $this->data['search'] ) : ?>
                                <?= preg_replace( "/({$this->data[search]})/iu", '<span class="search_highlight">$1</span>', $aData['nazwa_' . LANG] ); ?>
                            <?php else : ?>
                                <?= $aData['nazwa_' . LANG]; ?>
                            <?php endif; ?>
                        </p>
                        
                        <div class="corp-detail-div">
                            <?php if( $aData['adres'] ) : ?>
                                <span class='corp-ico'>
                                    <i class="icon-home"></i>
                                    <?= $aData['adres']; ?><br><?= $aData['kod']; ?> <?= $aData['miasto']; ?>
                                </span>
                            <?php endif; ?>
                            <br>
                            <?php if( $aData['telefon'] ) : ?>
                                <span class='corp-ico'>
                                    <i class="icon-phone"></i>
                                    <a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $aData['telefon'] ); ?>" target='_blank'><?= $aData['telefon']; ?></a>
                                </span>
                            <?php endif; ?>
                            <?php if( $aData['email'] ) : ?>
                                <span class='corp-ico'>
                                    <i class="icon-mail"></i>
                                    <a href="mailto:<?= $aData['email']; ?>" target='_blank'><?= $aData['email']; ?></a>
                                </span>
                            <?php endif; ?>
                            <?php if( $aData['fax'] ) : ?>
                                <span class='corp-ico'>
                                    <i class="icon-print"></i>
                                    <?= $aData['fax']; ?>
                                </span>
                            <?php endif; ?>
                            <?php if( $aData['www'] ) : ?>
                                <span class='corp-ico'>
                                    <i class="icon-monitor"></i>
                                    <a href='<?php if( preg_match( "/^(http:\/\/)/", $aData['www'] ) ) { echo $aData['www']; } else { echo "http://" . $aData['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $aData['www'] ); ?></a>
                                </span>
                            <?php endif; ?>
                        </div>
<?php /*
                        <table class="corp-datail">
                            <tbody>
                                <tr>
                                    <td class='td-ico'><i class="icon-home"></i></td>
                                    <td><?= $aData['adres']; ?><br><?= $aData['kod']; ?> <?= $aData['miasto']; ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class='td-ico'><i class="icon-phone"></i></td>
                                    <td><a href="tel:+48<?= preg_replace( "/[\-\s_*]/", '', $aData['telefon'] ); ?>" target='_blank'><?= $aData['telefon']; ?></a></td>
                                    <td class='td-ico'><i class="icon-mail"></i></td>
                                    <td>
                                        <a href="mailto:<?= $aData['email']; ?>" target='_blank'><?= $aData['email']; ?></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class='td-ico'><i class="icon-print"></i></td>
                                    <td><?= $aData['fax']; ?></td>
                                    <td class='td-ico'><i class="icon-monitor"></i></td>
                                    <td>
                                        <a href='<?php if( preg_match( "/^(http:\/\/)/", $aData['www'] ) ) { echo $aData['www']; } else { echo "http://" . $aData['www']; } ?>' target='_blank'><?= preg_replace( "/(http:\/\/|https:\/\/)/" , "", $aData['www'] ); ?></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
*/ ?>
                    </div>
                    <?php if( !$aData['zajawka_' . LANG] ) : ?>
                        <div class="link-group"><a href="<?= BASE; ?>firmy/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][19]['pl']; ?></a></div>
                    <?php endif; ?>
                </div>
                <?php if( $aData['zajawka_' . LANG] ) : ?>
                    <div class="event-i-det-c">
                        <?= $aData['zajawka_' . LANG]; ?>
                        <div class="link-group"><a href="<?= BASE; ?>firmy/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>" class='more'><?= $this->data['dictionary'][19]['pl']; ?></a></div>
                    </div>
                <?php endif; ?>


            <?php endforeach; ?>
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>
        <div class="row">
            <?= $page; ?>
        </div>
    </div>
    <!-- ! lewa strona -->

    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>