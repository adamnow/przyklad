<!doctype html>
    <!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pl"> <![endif]-->
    <!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="pl"> <![endif]-->
    <!--[if IE 8]><html class="no-js lt-ie9" lang="pl"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js<?= $adminClass; ?>" lang="pl"> <!--<![endif]-->

    <head>
        <title>SEC&AS<?= $this->data['seo']['title']; ?></title>
        <meta name="description" content="<?= $this->data['seo']['description']; ?>" />
        <meta name="keywords" content="<?= $this->data['seo']['keywords']; ?>" />
        <meta name='author' content='<?= $this->data['seo']['author']; ?>' />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-language" content="pl" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name='robots' content='index, follow' />
        <meta property='og:title' content='SEC&AS<?= $this->data['seo']['title']; ?>' />
        <meta property='og:description' content='<?= $this->data['seo']['description']; ?>' />
        <meta property='og:url' content='<?= $this->data['seo']['url']; ?>' />
        <meta property='og:site_name' content='<?= $this->data['dictionary'][51][LANG]; ?>' />
        <meta property='og:type' content='<?= $this->data['seo']['type']; ?>' />
        <meta property="og:image" content="<?= $this->data['seo']['image']; ?>" />
        <meta property="og:image:type" content="image/jpeg" />

        <?php if( Routing::$routing['controller'] == 'index' && Routing::$routing['action'] == 'widok' ) : ?>
            <link rel="canonical" href="<?= BASE; ?>" />
        <?php elseif( Routing::$routing['action'] == 'widok' ) : ?>
            <link rel="canonical" href="<?= BASE; ?><?= Routing::$routing['controller']; ?>" />
        <?php endif; ?>

        <link rel="apple-touch-icon" sizes="57x57" href="<?= BASE; ?>public/icons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= BASE; ?>public/icons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= BASE; ?>public/icons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= BASE; ?>public/icons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= BASE; ?>public/icons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= BASE; ?>public/icons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= BASE; ?>public/icons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= BASE; ?>public/icons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= BASE; ?>public/icons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= BASE; ?>public/icons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= BASE; ?>public/icons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= BASE; ?>public/icons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= BASE; ?>public/icons/favicon-16x16.png">
        <link rel="manifest" href="<?= BASE; ?>public/icons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= BASE; ?>public/icons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
    
        <link rel="stylesheet" href="<?= BASE; ?>public/scss/reset.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/scss/bootstrap.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/scss/bootstrap-theme.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/fontello/css/fontello.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/fontello/css/animation.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/plugins/slick/slick.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/plugins/slick/slick-theme.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/plugins/magnificpopup/dist/magnific-popup.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/plugins/datepicker/dist/datepicker.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?= BASE; ?>public/scss/style.css" type="text/css" media="all" />
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-91823887-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body>
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        
            <!-- top -->
            <header id='header'>
                <button id="open-nav"><i class="icon-menu"></i></button>
                <div class="container">
                    <div class="row">
                        <!-- logo -->
                        <div class="col-sm-3">
                            <figure>
                                <a href="<?= BASE; ?>"><img src="<?= BASE; ?>public/images/secandas-logo.png" alt=""></a>
                            </figure>
                           <a href="http://www.replicas-rolex.co" style="text-decoration:none; color:#fff">rolex replica watches</a>
                        </div>
                        <!-- ! logo -->
                        <!-- nawigacje -->
                        <div class="col-sm-9">
                            <div class="row">
                                <nav id="main-nav">
                                    <?php if( $this->data['menu'] ) : ?>
                                        <div class="nav-links">
                                            <a href="<?= BASE; ?>czasopismo" <?php if( Routing::$routing['controller'] == 'czasopismo' ) echo 'class="act"'; ?>><?= $this->data['dictionary'][139][LANG]; ?></a>
                                            <?php foreach( $this->data['menu'] as $aData ) : ?>
                                                <?php $active = ( Routing::$routing['controller'] == 'strona' && Routing::$routing['param'] == $aData['id'] ) ? "class='act'" : ""; ?>
                                                <a <?= $active; ?> href="<?= BASE; ?>strona/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_' . LANG] ); ?>"><?= $aData['tytul_' . LANG]; ?></a>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="nav-search">
                                        <form id="top-search" action='<?= BASE; ?>index/szukaj'>
                                            <fieldset>
                                                <input type="search" required name="szukaj" <?php if( $_GET['szukaj'] ) echo "value='" . strip_tags( trim( $_GET['szukaj'] ) ) . "'" ?>>
                                                <button><i class="icon-search"></i></button>
                                                <a href="<?= $this->data['dictionary'][22]['pl']; ?>" target='_blank'><i class="icon-facebook"></i></a>
                                            </fieldset>
                                        </form>
                                    </div>
                                </nav>
                                <?php $navOptions = ['navMargin' => '100px', 'subnavTop' => '195px']; ?>
                                <?php if( $this->data['adv_top'] ) : ?>
                                    <?php 
                                        $navOptions = ['navMargin' => '0', 'subnavTop' => '228px'];
                                    ?>
                                    <figure id="top-adv">
                                        <a href="<?= BASE; ?>reklama/widok/<?= $this->data['adv_top'][0]['id']; ?>/<?= Helper::uri_string( $this->data['adv_top'][0]['tytul_pl'] ); ?>" target='_blank'><img src="<?= BASE; ?>userfiles/images/rekl/<?= $this->data['adv_top'][0]['image']; ?>" alt=""></a>
                                    </figure>
                                <?php endif; ?>
                                <nav id="nav" style='margin-top: <?= $navOptions['navMargin']; ?>;'>
                                    <button id='back-1'><i class="icon-angle-left"></i></button>
                                    <div class="nav-item">
                                        <?php if( $this->data['articleCategories'] ) : ?>
                                            <a href="<?= BASE; ?>artykuly" data-subnav='1' <?php if( Routing::$routing['controller'] == 'artykuly' || Routing::$routing['controller'] == 'artykul' ) echo "class='act'"; ?>><?= $this->data['dictionary'][11][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php else : ?>
                                            <a href="<?= BASE; ?>artykuly" <?php if( Routing::$routing['controller'] == 'artykuly' || Routing::$routing['controller'] == 'artykul' ) echo "class='act'"; ?>><?= $this->data['dictionary'][11][LANG]; ?></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="nav-item">
                                        <?php if( $this->data['articleCategories'] ) : ?>
                                            <a href="<?= BASE; ?>newsy" data-subnav='4' <?php if( Routing::$routing['controller'] == 'newsy' || Routing::$routing['controller'] == 'news' ) echo "class='act'"; ?>><?= $this->data['dictionary'][137][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php else : ?>
                                            <a href="<?= BASE; ?>newsy" <?php if( Routing::$routing['controller'] == 'newsy' || Routing::$routing['controller'] == 'news' ) echo "class='act'"; ?>><?= $this->data['dictionary'][137][LANG]; ?></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="nav-item">
                                        <?php if( $this->data['eventCategories'] ) : ?>
                                            <a href="<?= BASE; ?>wydarzenia/wszystkie" data-subnav='2' <?php if( Routing::$routing['controller'] == 'wydarzenia' ) echo "class='act'"; ?>><?= $this->data['dictionary'][12][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php else : ?>
                                            <a href="<?= BASE; ?>wydarzenia/wszystkie" <?php if( Routing::$routing['controller'] == 'wydarzenia' ) echo "class='act'"; ?>><?= $this->data['dictionary'][12][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="nav-item no-submenu">
                                        <a href="<?= BASE; ?>firmy/wszystkie" <?php if( Routing::$routing['controller'] == 'firmy' ) echo "class='act'"; ?>><?= $this->data['dictionary'][57][LANG]; ?> <i class="icon-angle-down"></i></a>
                                    </div>
                                    <div class="nav-item no-submenu">
                                        <a href="<?= BASE; ?>strona/widok/1/czasopismo" <?php if( Routing::$routing['controller'] == 'strona' && Routing::$routing['param'] == 1 ) echo "class='act'"; ?>><?= $this->data['dictionary'][54][LANG]; ?> <i class="icon-angle-down"></i></a>
                                    </div>
                                    <div class="nav-item">
                                        <?php if( $this->data['articleCategories'] ) : ?>
                                           <a href="<?= BASE; ?>newsroom/wszystkie" data-subnav='3' <?php if( Routing::$routing['controller'] == 'newsroom' ) echo "class='act'"; ?>><?= $this->data['dictionary'][23][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php else : ?>
                                           <a href="<?= BASE; ?>newsroom/wszystkie" <?php if( Routing::$routing['controller'] == 'newsroom' ) echo "class='act'"; ?>><?= $this->data['dictionary'][23][LANG]; ?> <i class="icon-angle-down"></i></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="nav-item no-submenu">
                                        <a href="<?= BASE; ?>eksperci/wszystkie" <?php if( Routing::$routing['controller'] == 'eksperci' ) echo "class='act'"; ?>><?= $this->data['dictionary'][140][LANG]; ?> <i class="icon-angle-down"></i></a>
                                    </div>
                                    <div class="nav-item">
                                        <a href="<?= BASE; ?>galeria/kategorie" <?php if( Routing::$routing['controller'] == 'galeria' ) echo "class='act'"; ?>><?= $this->data['dictionary'][129][LANG]; ?> <i class="icon-angle-down"></i></a>
                                    </div>
                                </nav>    
                            </div>
                        </div>
                        <!-- ! nawigacje -->
                    </div>
                    <!-- podkategorie w nawigacji -->
                    <?php if( $this->data['articleCategories'] ) : ?>
                        <div id="subnav">
                           <div class="subnav" data-subnav='1'  style='top: <?= $navOptions['subnavTop']; ?>;'>
                                <button class="back-2"><i class="icon-angle-left"></i></button>
                                <?php foreach( $this->data['articleCategories'] as $aData ) : ?>
                                   <a href="<?= BASE ?>artykuly/kategoria/<?= $aData['_ID_']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>"><?= $aData['nazwa_' . LANG]; ?></a>
                                <?php endforeach; ?>
                           </div>
                        </div>
                    <?php endif; ?>
                    <?php if( $this->data['eventCategories'] ) : ?>
                        <div class="subnav" style='top: <?= $navOptions['subnavTop']; ?>;' data-subnav='2'>
                            <button class="back-2"><i class="icon-angle-left"></i></button>
                            <?php foreach( $this->data['eventCategories'] as $aData ) : ?>
                                <a href="<?= BASE; ?>wydarzenia/wszystkie/?f[cat]=<?= $aData['id']; ?>&f[n]=<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>"><?= $aData['nazwa_' . LANG]; ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if( $this->data['newsroomCategories'] ) : ?>
                        <div id="subnav">
                           <div class="subnav" data-subnav='3'  style='top: <?= $navOptions['subnavTop']; ?>;'>
                                <button class="back-2"><i class="icon-angle-left"></i></button>
                                <?php foreach( $this->data['newsroomCategories'] as $aData ) : ?>
                                   <a href="<?= BASE ?>newsroom/kategoria/<?= $aData['_ID_']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>"><?= $aData['nazwa_' . LANG]; ?></a>
                                <?php endforeach; ?>
                           </div>
                        </div>
                    <?php endif; ?>
                    <?php if( $this->data['articleCategories'] ) : ?>
                        <div id="subnav">
                           <div class="subnav" data-subnav='4'  style='top: <?= $navOptions['subnavTop']; ?>;'>
                                <button class="back-2"><i class="icon-angle-left"></i></button>
                                <?php foreach( $this->data['articleCategories'] as $aData ) : ?>
                                   <a href="<?= BASE ?>newsy/kategoria/<?= $aData['_ID_']; ?>/<?= Helper::uri_string( $aData['nazwa_' . LANG] ); ?>"><?= $aData['nazwa_' . LANG]; ?></a>
                                <?php endforeach; ?>
                           </div>
                        </div>
                    <?php endif; ?>
                    </div>
                    <!-- ! podkategorie w nawigacji -->
            </header>
            <!-- ! top -->

        <!-- kontent -->
        <section class="content mainpage-content">
                <div class="container">

                    <?php 
                        try {
                            $this->renderView(); 
                        } catch (viewException $e) {
                            Error::setMessage( 'template_file', $e->getMessage() . ' kod błędu:' . $e->getCode(), 'error' );
                            $this->template_file = Helper::writeError404();
                            $this->renderView(); 
                        }
                    ?>

                </div>
        </section>
        <!-- ! kontent -->

            <!-- stopa -->
            <footer>
                <div class="container">
                    <div class="row">
                        <nav class="col-md-4">
                            <a href="<?= BASE; ?>artykuly"><?= $this->data['dictionary'][11][LANG]; ?></a>
                            <a href="<?= BASE; ?>newsy"><?= $this->data['dictionary'][137][LANG]; ?></a>
                            <a href="<?= BASE; ?>wydarzenia/wszystkie"><?= $this->data['dictionary'][12][LANG]; ?></a>
                            <a href="<?= BASE; ?>firmy/wszystkie"><?= $this->data['dictionary'][57][LANG]; ?></a>
                            <a href="<?= BASE; ?>strona/widok/1/czasopismo"><?= $this->data['dictionary'][54][LANG]; ?></a>
                            <a href="<?= BASE; ?>newsroom/wszystkie"><?= $this->data['dictionary'][23][LANG]; ?></a>
                            <a href="<?= BASE; ?>eksperci/wszystkie"><?= $this->data['dictionary'][140][LANG]; ?></a>
                            <a href="<?= BASE; ?>galeria/kategorie"><?= $this->data['dictionary'][129][LANG]; ?></a>
                            <a href="<?= $this->data['dictionary'][22]['pl']; ?>" id='fb-link' target='_blank'><i class="icon-facebook"></i></a>
                        </nav>
                        <div class="col-md-4">
                            <h5><?= $this->data['dictionary'][131][LANG]; ?></h5>
                            <p><?= $this->data['dictionary'][130][LANG]; ?></p>
                        </div>
                        <div class="col-md-4">
                            <h5><?= $this->data['dictionary'][132][LANG]; ?></h5>
                            <p><?= $this->data['dictionary'][133][LANG]; ?></p>
                            <form method='post' action='<?= BASE; ?>newsletter'>
                                <input type="email" name='newsletter' required placeholder='<?= $this->data['dictionary'][134][LANG]; ?>'>
                                <button name='send-newsletter' value='1' ><i class="icon-angle-right"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copy">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            COPYRIGHT © 2017 RIPOSTA. WSZELKIE PRAWA ZASTRZEŻONE. PROJEKT I REALIZACJA: <a href="http://riposta.pl" target='_blank'>RIPOSTA</a>
<a href="http://www.replicavvatches.co.uk" style="text-decoration:none; color:#fff">replica watches uk</a>&nbsp;
<a href="http://www.vogueluxury.ru" style="text-decoration:none; color:#fff">replica handbags</a>
                        </div>
                        <?php if( $this->data['menu'] ) : ?>
                            <div class="col-sm-6 text-right">
                                <a href="<?= BASE; ?>czasopismo" <?php if( Routing::$routing['controller'] == 'czasopismo' ) echo 'class="act"'; ?>>Prenumerata</a>
                                <?php foreach( $this->data['menu'] as $aData ) : ?>
                                    <?php $active = ( Routing::$routing['controller'] == 'strona' && Routing::$routing['param'] == $aData['id'] ) ? "class='act'" : ""; ?>
                                    <a <?= $active; ?> href="<?= BASE; ?>strona/widok/<?= $aData['id']; ?>/<?= Helper::uri_string( $aData['tytul_' . LANG] ); ?>"><?= $aData['tytul_' . LANG]; ?></a>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>                                    
                    </div>
                </div>
            </div>
            <!-- ! stopa -->

            <div class="cookie-alert" style="display: none;"><div class="inner-cookie"><div class="message">PLIKI COOKIES: Stosujemy pliki cookies, aby ułatwić korzystanie z serwisu. Szczegóły znajdziesz w <a href="<?= BASE; ?>strona/widok/7/polityka-cookies" title="Polityka Cookies"><em>Polityce Cookies</em></a>.</div><div class="cookie-alert2">Zamknij</div></div></div>

        <script src="<?= BASE; ?>public/js/jquery-1.11.1.min.js"></script>
        <script src="<?= BASE; ?>public/js/bootstrap.min.js"></script>
        <script src='<?= BASE; ?>public/js/babel.6.24.0.min.js'></script>
        <script src='<?= BASE; ?>public/js/react.min.js'></script>
        <script src='<?= BASE; ?>public/js/react-dom.min.js'></script>
        <script src='<?= BASE; ?>public/plugins/slick/slick.min.js'></script>
        <script src='<?= BASE; ?>public/plugins/magnificpopup/dist/jquery.magnific-popup.min.js'></script>
        <script src='<?= BASE; ?>public/plugins/datepicker/dist/datepicker.min.js'></script>
        <script src='<?= BASE; ?>public/plugins/masonry/masonry.js'></script>
        <script src='<?= BASE; ?>public/plugins/cementjs/cement.min.js'></script>
        <script>
            $('.date').datepicker({
                format : 'yyyy-mm-dd',
                autoHide : true,
            });

            $(window).on('load',function() {
                // $('.gallery-detail').cement();
                $('.gallery-detail').masonry({
                    // set itemSelector so .grid-sizer is not used in layout
                    itemSelector: '.gallery-detail > div',
                    // use element for option
                    percentPosition: true
                })
            })
        </script>
        <script src="<?= BASE; ?>public/js/_main.js" type="text/babel"></script>

    </body>
</html>