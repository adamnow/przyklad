<?php
    exit();
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
    <div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show">
        <h1>Newsroom</h1>
        <span class="article-date">&nbsp;</span>
        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <?php if( $this->data['newsroom_art'] ) : ?>
            <?php foreach( $this->data['newsroom_art'] as $aData ) : ?>
                <div class="event-i experts-i">
                    <figure class="col-sm-4">
                        <?php if( @file_get_contents( 'userfiles/images/newsroom/' . $aData['image'] ) ) : ?>
                            <img src="<?= BASE; ?>userfiles/images/newsroom/<?= $aData['image']; ?>" alt="">
                        <?php else : ?>
                            <div class="no-image"></div>
                        <?php endif; ?>
                    </figure>
                    <div class="col-sm-8 e-txt">
                        <p class="e-title"><?= $aData['tytul_' . LANG]; ?></p>
                        <p class="e-date">Data publikacji: <strong><?= $aData['data']; ?></strong> Biuro prasowe: <strong><?= $aData['biuro']['firma']; ?></strong></p>
                        <p class="e-ctn"><?= strip_tags( $aData['zajawka_' . LANG] ); ?></p>
                        <div class="link-group no-margin-b">
                            <?php if( $aData['biuro']['www'] ) : ?>
                                <a href="<?= $aData['biuro']['www']; ?>" target='_blank' class='more more-gray'>Biuro prasowe</a>
                            <?php endif; ?>
                            <a href="<?= BASE; ?>newsroom/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>" class='more'>Czytaj całosc</a>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <?php require 'libs/templates/_elements/_no_data.php'; ?>
        <?php endif; ?>
        <div class="row">
            <?= $page; ?>
        </div>
    </div>
    <!-- ! lewa strona -->

    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>