<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
    <div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show">
        <h1><?= $this->data['dictionary'][23][LANG]; ?></h1>
        <span class="article-date">&nbsp;</span>
        <?php $page = Helper::pagination( array( 'current' => (int)$_GET['p'], 'allpages' => ceil( $this->data['TOTAL'] / PERPAGE ) ) ); ?>
        <?php if( $this->data['newsroom_art'] ) : ?>
            <?php foreach( $this->data['newsroom_art'] as $aData ) : ?>
                <div class="event-i">
                    <figure class="col-sm-4">
                        <?php if( @file_get_contents( 'userfiles/images/newsroom/' . $aData['image'] ) ) : ?>
                            <img src="<?= BASE; ?>userfiles/images/newsroom/<?= $aData['image']; ?>" alt="">
                        <?php else : ?>
                            <div class="no-image"></div>
                        <?php endif; ?>
                    </figure>
                    <div class="col-sm-8 e-txt">
                        <p class="e-title"><?= $aData['tytul_' . LANG]; ?></p>
                        <p class="e-date"><?= $this->data['dictionary'][66][LANG]; ?> <strong><?= $aData['data']; ?></strong> 
                        <?php if( $aData['biuro'] ) : ?>
                            <?= $this->data['dictionary'][91][LANG]; ?>: <strong><?= $aData['biuro']['nazwa_pl']; ?></strong></p>
                        <?php endif; ?>
                        <p class="e-ctn"><?= strip_tags( $aData['zajawka_' . LANG] ); ?></p>
                    </div>
                    <div class="link-group no-margin-b">
                        <?php if( $aData['biuro']['www'] ) : ?>
                            <!-- <a href="<?= $aData['biuro']['www']; ?>" target='_blank' class='more more-gray'><?= $this->data['dictionary'][91][LANG]; ?></a> -->
                            <a href="<?= BASE; ?>firmy/widok/<?= $aData['biuro']['id']; ?>/<?= Helper::uri_string( $aData['biuro']['nazwa_pl'] ); ?>" target='_blank' class='more more-gray'><?= $this->data['dictionary'][91][LANG]; ?></a>
                        <?php endif; ?>
                        <a href="<?= BASE; ?>newsroom/widok/<?= $aData['id']; ?>/<?= explode( '.' , $aData['image'] )[0]; ?>" class='more'><?= $this->data['dictionary'][64][LANG]; ?></a>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <?php // require 'libs/templates/_elements/_no_data.php'; ?>
            <p>PRESSROOM - portal umożliwia firmom branżowym przedstawienie swoich nowych rozwiązań oraz kreowanie własnego wizerunku za pośrednictwem przejrzystej zakładki Pressroom, która niebawem zostanie uruchomiona na naszej platformie. Zakładka ta będzie umożliwiać zamieszczenie treści firmowych po zalogowaniu się za pomocą przypisanego firmie hasła. Funkcja ta będzie odpłatna, a dla firm zrzeszonych w PISA przewidzieliśmy specjalne rabaty.</p>
        <?php endif; ?>
        <div class="row">
            <?= $page; ?>
        </div>
    </div>
    <!-- ! lewa strona -->

    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>