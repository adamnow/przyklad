<?php
    Helper::infoSystem( [ 'type' => 'success', 'index' => 'login_success', 'message' => $this->data['dictionary'][23][LANG] ] );
    Helper::infoSystem( [ 'type' => 'success', 'index' => I_SUCCESS, 'message' => $_SESSION[I_SUCCESS] ] );
    Helper::infoSystem( [ 'type' => 'error', 'index' => I_ERROR, 'message' => $_SESSION[I_ERROR] ] );
    Helper::infoSystem( [ 'type' => 'info', 'index' => I_INFO, 'message' => $_SESSION[I_INFO] ] );
?>

<?php if( Error::getMessageItem( 'error_action_model' ) ) : ?>
	<div class="system-info system-info-error">
        <div>
            <?= Error::getMessageItem( 'error_action_model' )['message']; ?>
        </div>
        <button class="close-btn"></button>
    </div>
<?php endif; ?>


<div class="row">
    <!-- lewa strona -->
    <div class="col-sm-8 left-s-ctn article-show">

        <h1><?= $this->data['dictionary'][55][LANG]; ?></h1>
        <span class="article-date">&nbsp;</span>
        <div class="article-ctn">
            <?php /* <figure><img src="userfiles/images/secandas/secandas.jpg" alt=""></figure> */ ?>
            <?php /*
            <div class="article-ctn-txt">
                <div class="article-ctn-short">
                    Ze szczególną satysfakcją ogłaszamy, że po dwóch miesiącach intensywnych prac możemy Państwu przekazać pierwszy numer czasopisma SEC&AS (SECURITY & ALARM SYSTEMS).
                </div>
            </div>
            */ ?>

            <div class="article-ctn-txt">
                <div class="article-ctn-long">
                    <?php /*
                    <p>
                        Dwumiesięcznik, którego wydawcą jest Polska Izba Systemów Alarmowych wchodzi na rynek wydawnictw branżowych z nieukrywaną nadzieją pozyskania Państwa życzliwości i sympatii. Nadzieją, której towarzyszy ambicja bycia czasopismem wyróżniającym się formą, gatunkiem dziennikarskim, a przede wszystkim treścią.
                    </p>
                    <p>
                        Dzień 9 marca 2017 r., w którym do siedziby PISA dotarło z drukarni 1500 egzemplarzy pierwszego numeru czasopisma, przejdzie do historii naszej organizacji jako dzień                  spełnienia nowych wyzwań i triumfu możliwego nad niemożliwym. 
                    </p>
                    <p>
                        Dla jego autorów, redakcji i wydawcy to także pierwszy dzień niecierpliwego oczekiwania na opinie i reakcje Czytelników. Miną jeszcze tygodnie zanim je poznamy, zanim                  bezwzględny, bezuczuciowy licznik prenumeraty i elektronicznego czytelnictwa zweryfikuje nasze przekonanie o potrzebie i trafności decyzji wydawniczej. 
                    </p>
                    <p>
                        Żeby ten sprawdzian był jak najbardziej obiektywny, pomocne może się okazać Państwa zainteresowanie rozpropagowaniem inicjatywy PISA wśród swoich kontrahentów <i>                  </i>                kooperantów. Wszędzie tam gdzie można się spodziewać zapotrzebowania na rzetelną, obiektywną i ekspercką wiedzę, na kontakt z autorami programów edukacyjnych w             dziedzinie bezpieczeństwa oraz technik i technologii służących jego zapewnianiu. 
                    </p>
                    
                    <p>Już niedługo pod tym adresem będziecie mogli Państwo nawigować po stronach portalu internetowego Waszego dwumiesięcznika. </p>
                    
                    <p>
                        Natomiast już dziś redakcja SEC&AS otwiera łamy dla ukrytych dotąd autorskich talentów ekspertów i pracowników dydaktycznych PISA, specjalistów różnych dziedzin naszej branży reprezentujących na co dzień firmy członkowskie, a także sympatyków działalności                  Polskiej Izby Systemów Alarmowych. SEC&AS tworzy nowe szanse dla ujawnienia potencjału twórczego drzemiącego w nas wszystkich, schowanego pomiędzy wskaźnikami sprzedaży produktów, projektów i usług, czy też wynikami analiz efektów inwestycyjnych w obszarze bezpieczeństwa. 
                    </p>
                    <p>
                        Drugi numer czasopisma poświęcimy w dużej mierze bezpieczeństwu infrastruktury energetycznej. Nieprzypadkowo, bo z racji przejęcia przez PISA od firmy NAFTOR organizacji w dniach 10-11 maja br. KONGRESU OCHRONY INFRASTRUKTURY ENERGETYCZNEJ INFRATECH OCHRONA. 
                    </p>
                    <p><a href="#">www.infratech.edu.pl </a></p>
                    <p>Pozwalamy sobie na przypomnienie naszego zobowiązania, naszego celu, deklaracji i przesłania: </p>
                    <p style='font-size: 30px; color: #e3000f; text-align: center;'>
                        CZASOPISMO DO CZYTANIA, <br>
                        A NIE TYLKO DO PRZEGLĄDANIA  
                    </p>
                    <p>Z życzeniami satysfakcji z lektury czasopisma, z nadziejami na Państwa pozostanie z nami, z wyrazami szacunku, </p>
                    <p>
                        <strong>Mirosław Krasnowski</strong> – prezes Zarządu, <br>
                        <strong>Henryk Dąbrowski</strong> – dyrektor Wydawnictwa, <br>
                        <strong>Katarzyna Dąbrowska</strong> – redaktor naczelna.
                    </p>
                    */ ?>
                    <div class="form-order">
                        <h2><strong><?= $this->data['dictionary'][143][LANG]; ?></strong></h2>
                        <p><?= $this->data['dictionary'][144][LANG]; ?></p>
                        <p><?= $this->data['dictionary'][145][LANG]; ?></p>
                        <ul>
                            <li><?= $this->data['dictionary'][146][LANG]; ?></li>
                            <li><?= $this->data['dictionary'][147][LANG]; ?></li>
                        </ul>
                        <p><?= $this->data['dictionary'][148][LANG]; ?></p>
                        <p><a href="<?= BASE; ?>userfiles/files/SEC&AS-Formularz-prenumeraty.pdf" download class='pdf'><?= $this->data['dictionary'][149][LANG]; ?></a></p>
                        <p><?= $this->data['dictionary'][150][LANG]; ?></p>
                        <p>
                            <strong><?= $this->data['dictionary'][151][LANG]; ?></strong><br>
                            <?= $this->data['dictionary'][152][LANG]; ?>
                        </p>

                        <p>
                            <strong><?= $this->data['dictionary'][153][LANG]; ?></strong><br>
                            <?= $this->data['dictionary'][154][LANG]; ?>
                        </p>
                        <p><?= $this->data['dictionary'][155][LANG]; ?></p>

                        <form method="post">
                            <h2><strong><?= $this->data['dictionary'][156][LANG]; ?></strong></h2>
                            <div>
                                <div class="select-form select-form-block">
                                    <select name="prenumerata" id="prenumerata" required="">
                                        <option value=""><?= $this->data['dictionary'][139][LANG]; ?></option>
                                        <option value="wersja papierowa"><?= $this->data['dictionary'][157][LANG]; ?></option>
                                        <option value="wersja papierowa dla członków PISA"><?= $this->data['dictionary'][158][LANG]; ?></option>
                                        <option value="wersja elektroniczna"><?= $this->data['dictionary'][159][LANG]; ?></option>
                                    </select>
                                    <span class="dropdown"><i class="icon-angle-down"></i></span>
                                </div>
                            </div>    
                            <p class="form-info">
                                <?= $this->data['dictionary'][160][LANG]; ?>
                            </p>
                            <div>
                                <div class="select-form select-form-block">
                                    <select name="okres" id="okres" required="">
                                        <option value=""><?= $this->data['dictionary'][161][LANG]; ?></option>
                                        <option value="roczna"><?= $this->data['dictionary'][162][LANG]; ?></option>
                                        <option value="odnawialna"><?= $this->data['dictionary'][163][LANG]; ?></option>
                                    </select>
                                    <span class="dropdown"><i class="icon-angle-down"></i></span>
                                </div>
                            </div>    
                            <p class="form-info"><?= $this->data['dictionary'][164][LANG]; ?></p>
                            <div>
                                <input type="text" name='count' required placeholder='LICZBA EGZEMPLARZY'>
                            </div>

                            <h2><strong>Dane do faktury</strong></h2>
                            <div>
                                <input type="text" name='firma' required placeholder='FIRMA'>
                            </div>
                            <p></p>
                            <div>
                                <input type="text" name='adres' required placeholder='ULICA, NUMER DOMU, NUMER LOKALU'>
                            </div>
                            <p></p>
                            <div>
                                <input type="text" name='miejscowosc' required placeholder='KOD I MIEJSCOWOŚĆ'>
                            </div>
                            <p></p>
                            <div>
                                <input type="text" name='nip' required placeholder='NIP'>
                            </div>
                            <p></p>
                            <div>
                                <input type="text" name='email' required placeholder='EMAIL'>
                            </div>
                            <p></p>
                            <div>
                                <input type="text" name='telefon' required placeholder='TELEFON'>
                            </div>

                            <h2><strong><?= $this->data['dictionary'][165][LANG]; ?></strong></h2>
                            <div><label for="inny_adres"><input type="checkbox" name='inny_adres' id='inny_adres' value='1'> <?= $this->data['dictionary'][166][LANG]; ?></label></div>
                            <p></p>

                            <div class='dis'>
                                <input type="text" disabled name='wysylka_dane' required placeholder='FIRMA/IMIĘ I NAZWISKO'>
                            </div>
                            <p></p>
                            <div class='dis'>
                                <input type="text" disabled name='wysylka_adres' required placeholder='ULICA, NUMER DOMU, NUMER LOKALU'>
                            </div>
                            <p></p>
                            <div class='dis'>
                                <input type="text" disabled name='wysylka_miejscowosc' required placeholder='KOD, MIEJSCOWOŚĆ'>
                            </div>
                            
                            <div class="checkbox">
                                <label for="zgoda">
                                    <input type="checkbox" required id='zgoda' value='1' name='zgoda'>
                                    <span></span>
                                    <?= $this->data['dictionary'][167][LANG]; ?>                             
                                </label>
                            </div>
                            <div class="btn-form">
                                <button name='send-btn' value='1'><?= $this->data['dictionary'][79][LANG]; ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- ! lewa strona -->
    <!-- prawa strona -->
    <div class="col-sm-4 right-s-ctn">
        <!-- popularne -->
        <?php require "libs/templates/_elements/mainpage_popular.php"; ?>
        <!-- ! popularne -->

        <!-- reklama 1 -->
        <?php require "libs/templates/_elements/mainpage_advert_1.php"; ?>
        <!-- ! reklama 1 -->

        <!-- sacandas -->
        <?php require "libs/templates/_elements/mainpage_secandas.php"; ?>
        <!-- ! sacandas -->

        <!-- znajdź nas na fb -->
        <?php require "libs/templates/_elements/mainpage_fb.php"; ?>
        <!-- ! znajdź nas na fb -->

        <!-- reklama 2 -->
        <?php require "libs/templates/_elements/mainpage_advert_2.php"; ?>
        <!-- ! reklama 2 -->

        <!-- newsroom -->
        <?php require "libs/templates/_elements/mainpage_newsroom.php"; ?>
        <!-- ! newsroom -->

        <!-- ekspert -->
        <?php require "libs/templates/_elements/mainpage_expert.php"; ?>
        <!-- ! ekspert -->

        <!-- reklama 3 -->
        <?php require "libs/templates/_elements/mainpage_advert_3.php"; ?>
        <!-- ! reklama 3 -->

    </div>
    <!-- ! prawa strona -->
    
</div>